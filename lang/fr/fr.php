<?php 

$LANG = array(
    'L_HOME'                                    => 'Accueil',
    'L_FILE'                                    => 'Le fichier',
    'L_FOLDER'                                  => 'Le dossier',
    'L_ALREADY_EXISTS'                          => 'existe déjà !',
    'L_TOO_BIG'                                 => 'trop volumineux',
    'L_CORRECTLY_RENAME'                        => 'renommé correctement.',
    'L_RENAME_FAILED'                           => 'Échec pour renommer',
    'L_DOESNOT_EXIST_OR_IS_NOT_AN_IMAGE'        => 'n\'existe pas ou n\'est pas une image.',
    'L_PICTURE_MODIFICATION'                    => 'Modification d\'une image ',
    'L_PICTURE_MODIFICATION_ERROR'              => 'Erreur lors de la modification de l\'image ',
    'L_PICTURE_RECORDING_ERROR'                 => 'Erreur lors de l\'enregistrement de l\'image ',
    'L_PICTURE_MODIFICATION_AND_RECORDING'      => 'Modification et enregistrement de l\'image ',
    'L_THUMB'                                   => ' la miniature',
    'L_FILE_LINKED_TO_STUDY'                    => 'Fichier lié à l\'étude :',
    'L_RENAME'                                  => 'Renommer',
    'L_HELP'                                    => 'Aide',
    'L_INVALID_TYPE_FILE'                       => 'Type de fichier non valide',
    'L_INSERT_IMAGE'                            => 'Insérer une image',
    'L_UPLOAD'                                  => 'Envoyer',
    'L_UPLOAD_ON_SERVER'                        => 'Envoyer une image sur le serveur',
    'L_IMAGE_URL'                               => 'Adresse',
    'L_ALTERNATE_TEXT'                          => 'Texte alternatif',
    'L_LAYOUT'                                  => 'Habillage',
    'L_BORDER'                                  => 'Bordure',
    'L_ALIGNMENT'                               => 'Alignement',
    'L_LEFT'                                    => 'Gauche',
    'L_RIGHT'                                   => 'Droite',
    'L_TEXTTOP'                                 => 'Haut du texte',
    'L_ABSMIDDLE'                               => 'Milieu absolu',
    'L_BASELINE'                                => 'Baseline',
    'L_ABSBOTTOM'                               => 'Bas absolu',
    'L_BOTTOM'                                  => 'Bas',
    'L_MIDDLE'                                  => 'Milieu',
    'L_TOP'                                     => 'Haut',
    'L_HORIZONTAL_SPACE'                        => 'Espace horizontal',
    'L_VERTICAL_SPACE'                          => 'Espace vertical',
    'L_CENTER'                                  => 'Centrer',
    'L_SELECT_IMAGE'                            => 'Choisir une image',
    'L_INVALID_TYPE_FILE'                       => 'Type de fichier non valide',
    'L_FILES'                                   => 'DOCUMENTS',
    'L_PICT'                                    => 'IMAGES',
    'L_WIDTH'                                   => 'Largeur',
    'L_HEIGHT'                                  => 'Hauteur',
    'L_SUBMIT_BUTTON'                           => 'Valider',
    'L_CANCEL'                                  => 'Annuler',
    'L_PARAMETERS'                              => 'Paramètres',
    'L_SAVE_ALL'                                => 'Enregistrer',
    'L_PREVIEW'                                 => 'Aperçu',
    'L_SLIDES'                                  => ' Diapositives',
    'L_SLIDE'                                   => ' Diapositive',
    'L_NOTES'                                   => ' Notes',
    'L_ADD_SLIDE'                               => 'Ajouter une diapositive',
    'L_GENERAL_TITLE'                           => 'Titre',
    'L_LANG'                                    => 'Langue',
    'L_AUTHOR'                                  => 'Auteur',
    'L_COMPANY'                                 => 'Entreprise',
    'L_MAIN_TITLE'                              => 'Titre Principal',
    'L_MEETING_PLACE'                           => 'Lieu de la présentation',
    'L_MEETING_DATE'                            => 'Date de la présentation',
    'L_COMPANY_LINK'                            => 'Lien vers le site de l\'entreprise',
    'L_ADMINISTRATIVE_PARAMETERS'               => 'Renseignements administratifs',
    'L_THEME'                                   => 'Thème',
    'L_COLORS'                                  => 'Couleurs',
    'L_REC'                                     => 'Enregistrer',
    'L_HEADER_BACKGROUND_COLOR'                 => 'Fond de l\'entête',
    'L_HEADER_FONT_COLOR'                       => 'Police de l\'entête',
    'L_MAIN_SECTION_BACKGROUND_COLOR'           => 'Fond du corps',
    'L_FIRST_TITLE_COLOR'                       => 'Titre de la première diapositive',
    'L_MAIN_TITLE_COLOR'                        => 'Titres',
    'L_MAIN_TITLE_BACKGROUND_COLOR'             => 'Fond des titres',
    'L_CURRENT_COLOR'                           => 'Couleur ligne active',
    'L_LINKS_COLOR'                             => 'Liens',
    'L_MAIN_FONT_COLOR'                         => 'Police du corps',
    'L_FOOTER_BACKGROUND_COLOR'                 => 'Fond du pied de page',
    'L_FOOTER_FONT_COLOR'                       => 'Police du pied de page',
    'L_CONTROLS_NAVLIST_COLOR'                  => 'Contrôles',
    'L_CONTROLS_NAVLIST_BACKGROUND_COLOR'       => 'Fond des contrôles',
    'L_CONTROLS_NAVLIST_LINKS_COLOR'            => 'Liens des contrôles',
    'L_CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR' => 'Fond des liens de contrôle',
    'L_CONTROLS_ACTIVE_COLOR'                   => 'Lien actif des contrôles',
    'L_CONTROLS_FOCUS_COLOR'                    => 'Lien avec le focus',
    'L_SLIDESNUMBERS_COLOR'                     => 'Numéros des diapositives',
    'L_CODE_COLOR'                              => 'Code',
    'L_HEADER_BORDER_BOTTOM_COLOR'              => 'Ligne inférieure de l\'entête',
    'L_FOOTER_BORDER_TOP_COLOR'                 => 'Ligne supérieure du pied',
    'L_STRONG_COLOR'                            => 'Emphase',
    'L_CONFIRM_DUPLICATE'                       => 'La diapositive sera placée en dernière position. Merci de confirmer la duplication de la diapositive',
    'L_ADD_NUMBER'                              => 'Merci de saisir un nombre entre 1 et ',
    'L_CONFIRM_DELETE'                          => 'Merci de confirmer la suppression de la diapositive',
    'L_DUPLICATE_SLIDE'                         => 'Dupliquer la diapositive',
    'L_RENAME_SLIDE'                            => 'Renommer la diapositive',
    'L_REMOVE_SLIDE'                            => 'Supprimer la diapositive',
    'L_SELECT_DOCUMENT'                         => 'Sélectionner un document',
    'L_UPLOAD_DOC_ON_SERVER'                    => 'Envoyer un document sur le serveur',
    'L_INSERT_DOCUMENT'                         => 'Insérer un document',
    'L_DOCUMENT_URL'                            => 'Adresse',
    
    'L_CREATE_OR_MODIFY_TABLE'                  => 'Créer ou modifier un tableau',
    'L_TABLE_PROPERTIES'                        => 'Propriétés du tableau',
    'L_ROWS'                                    => 'Lignes',
    'L_COLS'                                    => 'Colonnes',
    'L_PADDING'                                 => 'Espacement interne',
    'L_BACKGROUND_COLOR'                        => 'Couleur de fond',
    'L_CHOOSE'                                  => 'Choisir',
    'L_BORDER_SIZE'                             => 'Taille de bordure',
    'L_BORDER_COLOR'                            => 'Couleur de bordure',
    'L_BORDER_STYLE'                            => 'Style de bordure',
    'L_SOLID'                                   => 'Solide',
    'L_DOUBLE'                                  => 'Double',
    'L_DOTTED'                                  => 'Pointillés',
    'L_DASHED'                                  => 'Tirets',
    'L_GROOVE'                                  => 'Groove',
    'L_RIDGE'                                   => 'Ridge',
    'L_INSET'                                   => 'Internes',
    'L_OUTSET'                                  => 'Externes',
    'L_BORDER_COLLAPSE'                         => 'Joindre les bordures',
    'L_CENTER'                                  => 'Centrer',
    'L_CREATE_OR_MODIFY_LINK'                   => 'Créer ou modifier un lien',
    'L_INSERT_HYPERLINK'                        => 'Insérer un lien',
    'L_TARGET'                                  => 'Cible',
    'L_BLANK'                                   => 'Nouvelle fenêtre',
    'L_SELF'                                    => 'Même fenêtre',
    'L_PARENT'                                  => 'Fenêtre parente',
    'L_TOP'                                     => 'Pop-up',
    'L_CUSTOM'                                  => 'Personnalisée',
    'L_STYLE'                                   => 'Style',
    'L_CLASS'                                   => 'Classe',
    'L_URL'                                     => 'URL',
    'EMAIL'                                     => 'Email',
    'L_BLANK'                                   => 'Nouvelle fenêtre',
    'L_NAME'                                    => 'Nom',
    'L_REQUIRED'                                => 'Obligatoire',
    'L_RESET'                                   => 'Réinitialiser',
    'L_DOWNLOAD'                                => 'Télécharger',
    'L_CONFIRM_RESET'                           => 'Merci de confirmer la réinitialisation. Toutes les données seront définitivement effacées.',
    'L_HELP_UNAVAILABLE'                        => 'Fichier d\'aide inexistant',
    ''                                          => '',
    ''                                          => '',
);