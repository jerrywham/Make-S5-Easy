<?php 
    // EDIT THIS DEFINITIONS IF YOU WANT //
    define('MS5E_LANG', 'fr');
    define('CHARSET', 'utf-8');


    // DON'T EDIT UNDER THIS LINE //
    # Définition des constantes d'adressage
    define('DS', DIRECTORY_SEPARATOR);
    define('MS5E_ROOT', dirname(__FILE__).DS);
    define('MS5E_TEMPLATE', MS5E_ROOT.'template'.DS);
    define('MS5E_TMP_PATH', MS5E_ROOT.'tmp'.DS);
    define('MS5E_LANG_PATH', MS5E_ROOT.'lang'.DS);
    define('MS5E_DATA_PATH', MS5E_ROOT.'data'.DS);
    define('MS5E_IMG_PATH', MS5E_DATA_PATH.'img'.DS);
    define('MS5E_MEDIAS_PATH', MS5E_DATA_PATH.'medias'.DS);
    define('MS5E_SLIDES_PATH', MS5E_DATA_PATH.'slides'.DS);
    define('MS5E_PRESENTATION_PATH', MS5E_DATA_PATH.'presentation'.DS);
    define('MS5E_FILES_PATH', MS5E_MEDIAS_PATH.'files'.DS);
    define('MS5E_PICT_PATH', MS5E_MEDIAS_PATH.'pict'.DS);
    define('MS5E_SCRIPTS', MS5E_ROOT.'javascript'.DS);
    define('BASE_URL', dirname(dirname($_SERVER['SCRIPT_NAME'])).'/');
    define('MS5E_session_domain',dirname(__FILE__));
    define('MS5E_FOOTER', 'Make-S5-Easy. Cyril MAGUIRE &copy 2017'.(date('Y') == 2017 ? '' :' - '.date('Y')));
    define('IMG_SHOW_PASS','data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAEJGlDQ1BJQ0MgUHJvZmlsZQAAOBGFVd9v21QUPolvUqQWPyBYR4eKxa9VU1u5GxqtxgZJk6XtShal6dgqJOQ6N4mpGwfb6baqT3uBNwb8AUDZAw9IPCENBmJ72fbAtElThyqqSUh76MQPISbtBVXhu3ZiJ1PEXPX6yznfOec7517bRD1fabWaGVWIlquunc8klZOnFpSeTYrSs9RLA9Sr6U4tkcvNEi7BFffO6+EdigjL7ZHu/k72I796i9zRiSJPwG4VHX0Z+AxRzNRrtksUvwf7+Gm3BtzzHPDTNgQCqwKXfZwSeNHHJz1OIT8JjtAq6xWtCLwGPLzYZi+3YV8DGMiT4VVuG7oiZpGzrZJhcs/hL49xtzH/Dy6bdfTsXYNY+5yluWO4D4neK/ZUvok/17X0HPBLsF+vuUlhfwX4j/rSfAJ4H1H0qZJ9dN7nR19frRTeBt4Fe9FwpwtN+2p1MXscGLHR9SXrmMgjONd1ZxKzpBeA71b4tNhj6JGoyFNp4GHgwUp9qplfmnFW5oTdy7NamcwCI49kv6fN5IAHgD+0rbyoBc3SOjczohbyS1drbq6pQdqumllRC/0ymTtej8gpbbuVwpQfyw66dqEZyxZKxtHpJn+tZnpnEdrYBbueF9qQn93S7HQGGHnYP7w6L+YGHNtd1FJitqPAR+hERCNOFi1i1alKO6RQnjKUxL1GNjwlMsiEhcPLYTEiT9ISbN15OY/jx4SMshe9LaJRpTvHr3C/ybFYP1PZAfwfYrPsMBtnE6SwN9ib7AhLwTrBDgUKcm06FSrTfSj187xPdVQWOk5Q8vxAfSiIUc7Z7xr6zY/+hpqwSyv0I0/QMTRb7RMgBxNodTfSPqdraz/sDjzKBrv4zu2+a2t0/HHzjd2Lbcc2sG7GtsL42K+xLfxtUgI7YHqKlqHK8HbCCXgjHT1cAdMlDetv4FnQ2lLasaOl6vmB0CMmwT/IPszSueHQqv6i/qluqF+oF9TfO2qEGTumJH0qfSv9KH0nfS/9TIp0Wboi/SRdlb6RLgU5u++9nyXYe69fYRPdil1o1WufNSdTTsp75BfllPy8/LI8G7AUuV8ek6fkvfDsCfbNDP0dvRh0CrNqTbV7LfEEGDQPJQadBtfGVMWEq3QWWdufk6ZSNsjG2PQjp3ZcnOWWing6noonSInvi0/Ex+IzAreevPhe+CawpgP1/pMTMDo64G0sTCXIM+KdOnFWRfQKdJvQzV1+Bt8OokmrdtY2yhVX2a+qrykJfMq4Ml3VR4cVzTQVz+UoNne4vcKLoyS+gyKO6EHe+75Fdt0Mbe5bRIf/wjvrVmhbqBN97RD1vxrahvBOfOYzoosH9bq94uejSOQGkVM6sN/7HelL4t10t9F4gPdVzydEOx83Gv+uNxo7XyL/FtFl8z9ZAHF4bBsrEwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAgpJREFUOBGFk01LG1EUhjOTxFUKgnWjUAhtJknJopDsijQBN61UIcTfoKvgvl0ILlwIXVRQ8A8IBbWS1GWC6wYKJZAPmkJBWzBxIZYgdjLxeaed0ETEAyfn633POXPvjdHv9313iYGoBuZOkDlKhuPPZDIBj+iRlVPtFt5rTtFtRux4oEgkMklstFqtttdoFOdOUncAtoixWGwefwl30o8oZ0UtJxqNtnF3qH1UTpxyuWwbyWQyWKlU/kAMU9xnwjj2vWmafsdxHmht4nOsg83Dvez1erlvSCqVCpoi032GQgM9rNfrYcAngDrEn1hil/gK/7TRaDzG3wsEAjUGZsQ1nyAUj9FFAKvxePwF/jQbVAHPoz9ouk0urkH4a2yygJaIYyYTCoBWKBwC0pU9xXxGF8i9rVarv5VHPlB7JodBRwxYxi3q5HXqg+sEZLJ+NxQKrZP3JRKJMVny7puQLxFOVj+vCd5ZlpVVAvlK95d8X1cBG1zLsmUO/SKfzxRni3jOwNHVPSdfQje09r9mOoeiCGDm0J/NZnOf716F+AadrdVqx4NrZNVHtm0fgJ8AvCki8vCv8Z3JQspTu8DNcg7fdY3uBt6jEIgJrwDqIU2hg++G+IuNdphaEM7juA2UgOQeCsChp6wab6ZD3v1D3cIp/7+C96fTaT3tobxyqo3mBxto0qgw7d6/8w1xE0QvGlqUMwAAAABJRU5ErkJggg==');
    #############################################
    #                                           #
    #  ░░░░   ░░░░  ░   ░░ ░░░░░░  ░░░░   ░░░░  #
    # ░░  ░░ ░░  ░░ ░░  ░░ ░░       ░░   ░░  ░░ #
    # ░░     ░░  ░░ ░░░ ░░ ░░       ░░   ░░     #
    # ░░     ░░  ░░ ░░░░░░ ░░░░░    ░░   ░░ ░░░ #
    # ░░     ░░  ░░ ░░ ░░░ ░░       ░░   ░░  ░░ #
    # ░░  ░░ ░░  ░░ ░░  ░░ ░░       ░░   ░░  ░░ #
    #  ░░░░   ░░░░  ░░   ░ ░░      ░░░░   ░░░░  #
    #                                           #
    #############################################
    //------- DO NOT EDIT. CAN BE EDIT WITH WEB INTERFACE -------//
    //------------------------ PARAMS ------------------------ //
    $LANG          = MS5E_LANG;
    $THEME         = 'default';
    $AUTHOR        = '';
    $COMPANY       = '';
    $COMPANY_LINK  = '';
    $TITLE         = '';
    $MAIN_TITLE    = '';
    $MEETING_PLACE = '';
    $MEETING_DATE  = date('d/m/Y');
    $SLIDES        = null;
    //------------------------ COLORS ------------------------//
    $HEADER_BACKGROUND_COLOR                 = 'rgba(10,138,188,0.8)';
    $HEADER_FONT_COLOR                       = 'rgba(255,169,33,1)';
    $HEADER_BORDER_BOTTOM_COLOR              = 'rgba(255,119,9,1)';
    $MAIN_SECTION_BACKGROUND_COLOR           = 'rgba(255,255,255,1)';
    $MAIN_FONT_COLOR                         = 'rgba(51,51,51,1)';
    $FIRST_TITLE_COLOR                       = 'rgba(10,138,188,0.8)';
    $MAIN_TITLE_COLOR                        = 'rgba(255,255,255,1)';
    $MAIN_TITLE_BACKGROUND_COLOR             = 'rgba(10,138,188,0)';
    $CURRENT_COLOR                           = 'rgba(176,43,2,1)';
    $LINKS_COLOR                             = 'rgba(10,138,188,0.8)';
    $FOOTER_BACKGROUND_COLOR                 = 'rgba(10,138,188,0.8)';
    $FOOTER_FONT_COLOR                       = 'rgba(255,255,255,1)';
    $FOOTER_BORDER_TOP_COLOR                 = 'rgba(255,149,3,1)';
    $CONTROLS_NAVLIST_COLOR                  = 'rgba(255,255,255,1)';
    $CONTROLS_NAVLIST_BACKGROUND_COLOR       = 'rgba(10,138,188,1)';
    $CONTROLS_NAVLIST_LINKS_COLOR            = 'rgba(2,57,71,1)';
    $CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR = 'rgba(10,138,188,0)';
    $CONTROLS_ACTIVE_COLOR                   = 'rgba(10,138,188,0.8)';
    $CONTROLS_FOCUS_COLOR                    = 'rgba(35,109,119,1)';
    $SLIDESNUMBERS_COLOR                     = 'rgba(11,153,84,1)';
    $CODE_COLOR                              = 'rgba(85,85,51,1)';
    $STRONG_COLOR                            = 'rgba(255,51,102,1)';
     //------- DO NOT EDIT. CAN BE EDIT WITH WEB INTERFACE -------//
    $slide_content = '';
    $notes_content = '';

    include MS5E_SLIDES_PATH.'colors.php';
    include MS5E_SLIDES_PATH.'params.php';

    $params_exists = trim(str_replace('<?php','',file_get_contents(MS5E_SLIDES_PATH.'params.php')));

    $aLang = initCache(MS5E_LANG_PATH,'dir',true);
    sort($aLang);
    $aTHEMES = initCache(MS5E_TEMPLATE.'themes','dir',true);
    sort($aTHEMES);

    loadLang($LANG.DS.$LANG.'.php');

    if (is_file(MS5E_LANG_PATH.$LANG.DS.$LANG.'-help.html')) {
        $help = file_get_contents(MS5E_LANG_PATH.$LANG.DS.$LANG.'-help.html');
    } else {
        if (is_file(MS5E_LANG_PATH.'fr'.DS.'fr-help.html')) {
            $help = file_get_contents(MS5E_LANG_PATH.'fr'.DS.'fr-help.html');
        } else {
            $help = '<p>'.L_HELP_UNAVAILABLE.'</p>';
        }
    }

    $slide_number = nextId(MS5E_SLIDES_PATH.'content');

    #########################################################  
    #                                                       #                                         
    # ██████████     ████████     ████████   ████████████   #
    # ████    ████ ████    ████ ████    ████ ██  ████  ██   #
    # ████    ████ ████    ████   ████           ████       #
    # ██████████   ████    ████     ████         ████       #
    # ████         ████    ████       ████       ████       #
    # ████         ████    ████ ████    ████     ████       #
    # ████           ████████     ████████     ████████     # 
    #                                                       #                                         
    #########################################################
    if (!empty($_POST) && strpos($_SERVER['REQUEST_URI'],'openwysiwyg') === false) {
        if (isset($_POST['number']) && !empty($_POST['number'])) {
            $slide_number = str_pad(intval($_POST['number']),4, '0', STR_PAD_LEFT);
        }
        include 'rec.php';
        $_POST = array();
        header('location:index.php');
        exit();
    }
    #########################################################                                                   
    #                                                       #                                         
    #         ████████   ████████████ ████████████          #
    #       ████    ████ ████         ██  ████  ██          #
    #       ████         ████             ████              #
    #       ████  ██████ ████████████     ████              #
    #       ████    ████ ████             ████              #
    #       ████    ████ ████             ████              #
    #         ████████   ████████████   ████████            #
    #                                                       #                                         
    #########################################################
    if (empty($params_exists) && !isset($_GET['firstConfig'])) {
        header('Location:parameters.php?firstConfig=true');
        exit();
    } else {
        if (!empty($_GET) && strpos($_SERVER['REQUEST_URI'],'openwysiwyg') === false && !isset($_GET['firstConfig'])) {
            include 'actions.php';
            $_GET = array();
            if (empty($slide_content) && empty($notes_content)) {
                header('location:index.php');
                exit();
            }
        }
    }
    ##############################
    #                             #
    # ░░░░░░  ░░░░  ░░░░   ░░░░░░ #
    # ░░       ░░    ░░    ░░     #
    # ░░       ░░    ░░    ░░     #
    # ░░░░░    ░░    ░░    ░░░░░  #
    # ░░       ░░    ░░    ░░     #
    # ░░       ░░    ░░ ░░ ░░     #
    # ░░      ░░░░  ░░░░░░ ░░░░░░ #
    #                             #
    ###############################
    if (is_file(MS5E_SLIDES_PATH.'footer.php')) {
        $footer = file_get_contents(MS5E_SLIDES_PATH.'footer.php');
    } else {
        $footer = null;
    }
    $slides = initCache(MS5E_SLIDES_PATH.'content','files',true);
    sort($slides);
    foreach ($slides as $key => $file) {
        $slide = file_get_contents(MS5E_SLIDES_PATH.'content'.DS.$file);
        $number = str_replace('.txt','',$file);
        $SLIDES .= '
        <div id="edit-'.$number.'" class="editor contextmenu" oncontextmenu="return monmenu(this)">
            <ul type="context" id="menu-'.$number.'" class="menu">
                <li class="menutitle">Make S-5 Easy</li>
                <li id="add-'.$number.'" class="add menuitem">'.L_ADD_SLIDE.'</li>
                <li id="duplicate-'.$number.'" class="duplicate menuitem" title="'.L_CONFIRM_DUPLICATE.'">'.L_DUPLICATE_SLIDE.'</li>
                <li id="rename-'.$number.'" class="rename menuitem" title="'.L_ADD_NUMBER.(intval(nextId(MS5E_SLIDES_PATH.'content'))-1).'">'.L_RENAME_SLIDE.'</li>
                <li id="remove-'.$number.'" class="remove menuitem" title="'.L_CONFIRM_DELETE.'">'.L_REMOVE_SLIDE.'</li>
            </ul>
            <div class="box">
                <h1 class="slide-number" id="slide-'.$number.'">'.intval($number).'</h1>
                <div class="thumb">
                    <div id="header"></div>
                    '.$slide.'
                    '.$footer.'
                </div>
            </div>
        </div>
        ';
    }


##################################################################
#                                                                #
# ▓▓▓▓▓▓ ▓▓  ▓▓ ▓   ▓▓  ▓▓▓▓  ▓▓▓▓▓▓  ▓▓▓▓   ▓▓▓▓  ▓   ▓▓  ▓▓▓▓  #
# ▓▓     ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ ▓ ▓▓ ▓   ▓▓   ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ #
# ▓▓     ▓▓  ▓▓ ▓▓▓ ▓▓ ▓▓       ▓▓     ▓▓   ▓▓  ▓▓ ▓▓▓ ▓▓  ▓▓    #
# ▓▓▓▓▓  ▓▓  ▓▓ ▓▓▓▓▓▓ ▓▓       ▓▓     ▓▓   ▓▓  ▓▓ ▓▓▓▓▓▓   ▓▓   #
# ▓▓     ▓▓  ▓▓ ▓▓ ▓▓▓ ▓▓       ▓▓     ▓▓   ▓▓  ▓▓ ▓▓ ▓▓▓    ▓▓  #
# ▓▓     ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓   ▓▓     ▓▓   ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ #
# ▓▓      ▓▓▓▓  ▓▓   ▓  ▓▓▓▓   ▓▓▓▓   ▓▓▓▓   ▓▓▓▓  ▓▓   ▓  ▓▓▓▓  #
#                                                                #
##################################################################

    function extractRGBA($color,$param) {
        $c = str_replace(array('rgba(',')'),'',$color);
        $params = explode(',',$c);
        switch ($param) {
            case 'R':
                return $params[0];
                break;
            case 'G':
                return $params[1];
                break;
            case 'B':
                return $params[2];
                break;
            case 'A':
                return $params[3];
                break;
            default:
                return $color;
                break;
        }
    }
 

    # fonction de chargement d'un fichier de langue
    function loadLang($filename) {
        if(file_exists(MS5E_LANG_PATH.$filename)) {
            $LANG = array();
            include_once(MS5E_LANG_PATH.$filename);
            foreach($LANG as $key => $value) {
                if(!defined($key)) define($key,$value);
            }
        } else {
            header("HTTP/1.0 404 Not Found");
            throw new Exception("File ".MS5E_LANG_PATH.$filename." not found", 404);
            exit();
        }
    }

    /**
     * Protège une chaine contre un null byte
     *
     * @param   string  $text chaine à nettoyer
     * @return  string chaine nettoyée
    */
    function nullbyteRemove($string) {
        return str_replace("\0", '', $string);
    }


    /**
     * Méthode qui retourne le niveau de sécurité du protocole utilisé
     *
     * @return bool true if https
     * @author Cyril MAGUIRE
     */
    function protocolIsHTTPS() {
        return (!empty($_SERVER['HTTPS']) AND $_SERVER['HTTPS'] == 'on') ? true : false;
    }

    /**
     * Méthode qui vérifie si l'url passée en paramètre correspond à un format valide
     *
     * @param   site        url d'un site
     * @return  boolean     vrai si l'url est bien formatée
     **/
    function checkSite(&$site, $reset=true) {

        $site = preg_replace('#([\'"].*)#', '', $site);
        # On vérifie le site via une expression régulière
        # Méthode Jeffrey Friedl - http://mathiasbynens.be/demo/url-regex
        # modifiée par Amaury Graillat pour prendre en compte la valeur localhost dans l'url
        if(preg_match('@\b((ftp|https?)://([-\w]+(\.\w[-\w]*)+|localhost)|(?:[a-z0-9](?:[-a-z0-9]*[a-z0-9])?\.)+(?: com\b|edu\b|biz\b|gov\b|in(?:t|fo)\b|mil\b|net\b|org\b|[a-z][a-z]\b))(\:\d+)?(/[^.!,?;"\'<>()\[\]{}\s\x7F-\xFF]*(?:[.!,?]+[^.!,?;"\'<>()\[\]{}\s\x7F-\xFF]+)*)?@iS', $site))
                return true;
        else {
            if($reset) $site='';
            return false;
        }
    }

    /**
     * Méthode qui retourne l'url de base du site
     *
     * @return  string      url de base du site
     **/
    function getRacine($truncate=false) {

        $protocol = protocolIsHTTPS() ? 'https://' : "http://";
        $servername = $_SERVER['HTTP_HOST'];
        $serverport = (preg_match('/:[0-9]+/', $servername) OR $_SERVER['SERVER_PORT'])=='80' ? '' : ':'.$_SERVER['SERVER_PORT'];
        $dirname = preg_replace('/\/(core|plugins)\/(.*)/', '', dirname($_SERVER['SCRIPT_NAME']));
        $racine = rtrim($protocol.$servername.$serverport.$dirname, '/').'/';
        $racine = str_replace(array('webroot/','install/'), '', $racine);
        if(!checkSite($racine, false))
            die('Error: wrong or invalid url');
        if ($truncate){ 
            $root = substr($racine,strpos($racine, '://')+3,strpos($racine,basename($racine))+4);
            $racine = substr($root,strpos($root,'/'));
        }
        $racine = str_replace(DS,'/',$racine);
        return $racine;
    }

    function getUrl($path) {

        $protocol = protocolIsHTTPS() ? 'https://' : "http://";
        $servername = $_SERVER['HTTP_HOST'];
        $serverport = (preg_match('/:[0-9]+/', $servername) OR $_SERVER['SERVER_PORT'])=='80' ? '' : ':'.$_SERVER['SERVER_PORT'];
        $basename = basename($_SERVER['DOCUMENT_ROOT']);
        $root = substr(MS5E_ROOT,strpos(MS5E_ROOT,$basename)+strlen($basename) );
        $path = str_replace(MS5E_ROOT,'',$path);
        return str_replace(DS,'/',$protocol.$servername.$serverport.$root.$path);
    }

    /**
     * Méthode qui formate une chaine de caractères en supprimant des caractères non valides
     *
     * @param   str         chaine de caracères à formater
     * @param   charset     charset à utiliser dans le formatage de la chaine (par défaut utf-8)
     * @return  string      chaine formatée
     */
    function removeAccents($str,$charset='utf-8') {

        $str = htmlentities($str, ENT_NOQUOTES, $charset);
        $str = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml|uro)\;#', '\1', $str);
        $str = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $str); # pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#\&[^;]+\;#', '', $str); # supprime les autres caractères
        return $str;
    }

    /**
     * Méthode qui convertit une chaine de caractères au format valide pour une url
     *
     * @param   str         chaine de caractères à formater
     * @return  string      nom d'url valide
     **/
    function title2url($str) {

        $str = strtolower(removeAccents($str,CHARSET));
        $str = preg_replace('/[^[:alnum:]]+/',' ',$str);
        return strtr(trim($str), ' ', '-');
    }

    /**
     * Méthode qui convertit une chaine de caractères au format valide pour un nom de fichier
     *
     * @param   str         chaine de caractères à formater
     * @return  string      nom de fichier valide
     **/
    function title2filename($str) {

        $str = nullbyteRemove($str);
        $str = strtolower(removeAccents($str,CHARSET));
        $str = str_replace('|','',$str);
        $str = preg_replace('/\.{2,}/', '.', $str);
        $str = preg_replace('/[^[:alnum:]|.|_]+/',' ',$str);
        return strtr(ltrim(trim($str),'.'), ' ', '-');
    }

    /**
     * Méthode qui va se charger de mémoriser le contenu d'un dossier
     *
     * @param   type            type de fichier lus (studies ou '')
     * @return  null
     * @author  Amaury Graillat et Stephane F
     **/
    function initCache($directory,$only='all',$basename=false) {
        $aDirs = array();
        $aFiles = array();
        $directory = rtrim($directory,'/');
        if(is_dir($directory)) {
            # On ouvre le repertoire
            if($dh = opendir($directory)) {
                # Récupération du dirname
                $dirname = $directory;  
                # Pour chaque entree du repertoire
                while(false !== ($file = readdir($dh))) {
                    if($file[0]!='.') {
                        $dir = is_dir($directory.'/'.$file);
                        if ($dir) {
                            $Ds = initCache($dirname.'/'.$file,'dir',true);
                            if ($basename) {
                                $aDirs[] = $file;
                                if (!empty($Ds)) {
                                    $aDirs[] = $file.'/'.current($Ds);
                                }
                            } else {
                                $aDirs[] = $directory.'/'.$file;
                                if (!empty($Ds)) {
                                    $aDirs[] = $directory.'/'.$file.'/'.current($Ds);
                                }
                            }
                            if (!$basename) {
                                $aFiles[][$file] = initCache($dirname.'/'.$file,'files');
                            } else {
                                $D = initCache($dirname.'/'.$file,'files',true);
                                if (is_dir($dirname.'/'.$file)) {
                                    $aFiles[$file] = $D;
                                } else {
                                    $aFiles[][$file] = $D;
                                }
                            }
                        }
                        elseif(!$dir) {
                            $aFiles[] = $file;
                        }
                    }
                }
                # On ferme la ressource sur le repertoire
                closedir($dh);
            }
        }
        switch ($only) {
            case 'dir':
               return $aDirs;
                break;
            case 'files':
                return $aFiles;
                break;
            case 'all':
                return array('dir' => $aDirs, 'files' => $aFiles);
                break;
        }
    }

    function replace(&$arg,$key,$params){
        $arg = str_replace($params[0],$params[1],$arg);
    }

    /**
     *  Méthode qui retourne le prochain id d'un article
     *
     * @return  string      id d'un nouvel article sous la forme 0001
     * @author  Stephane F.
     **/
    function nextId($dir) {

        if($aVal = initCache($dir,'files')) {
            array_walk($aVal,'replace',array('.txt',''));
            rsort($aVal);
            return str_pad($aVal['0']+1,4, '0', STR_PAD_LEFT);
        } else {
            return '0001';
        }
    }

    function reIndexSlides() {
        $slides = initCache(MS5E_SLIDES_PATH.'content','files');
        sort($slides);
        $i = 1;
        foreach ($slides as $key => $slide) {
            rename(MS5E_SLIDES_PATH.'content'.DS.$slide,MS5E_SLIDES_PATH.'content'.DS.str_pad($i,4, '0', STR_PAD_LEFT).'.txt');
            $i++;
        }
    }

    function recSlider($TITLE,$LANG,$AUTHOR,$COMPANY,$MAIN_TITLE,$MEETING_PLACE,$COMPANY_LINK) {
        $template = file_get_contents(MS5E_TEMPLATE.'index.slidetpl');
        $footer = file_get_contents(MS5E_SLIDES_PATH.'footer.php');
        $name = title2filename($TITLE);
        $racine = getRacine().'data/presentation/';
        $slides = '';
        $aSlides = initCache(MS5E_SLIDES_PATH.'content','files',true);
        sort($aSlides);
        foreach ($aSlides as $key => $file) {
            $slides .= file_get_contents(MS5E_SLIDES_PATH.'content'.DS.$file);
        }
        $template = str_replace(
            array('{{LANG}}','{{TITLE}}','{{AUTHOR}}','{{COMPANY}}','{{MAIN_TITLE}}','{{MEETING_PLACE}}','{{COMPANY_LINK}}','{{FOOTER}}','{{SLIDES}}'),
            array($LANG,$TITLE,$AUTHOR,$COMPANY,$MAIN_TITLE,$MEETING_PLACE,$COMPANY_LINK,$footer,$slides),
            $template
        );
        file_put_contents(MS5E_PRESENTATION_PATH.$name.'.html',$template);
    }

########################
#                      #
# ░░░░░░  ░░░░  ░░░░░  #
# ░░  ░░   ░░   ░░  ░░ #
#    ░░    ░░   ░░  ░░ #
#   ░░     ░░   ░░░░░  #
#  ░░      ░░   ░░     #
# ░░  ░░   ░░   ░░     #
# ░░░░░░  ░░░░  ░░     #
#                      #
########################
/**
 ****************************************
 * Description
 * @name        : BZN_zip_class.php
 * @Package     : BZN_
 * @Author      : Bronco
 * @Description : handles zip/unzip
 * @Version     : 1.0
 ****************************************
 **/

class zip{
    
    private $zip_file;

    public function __construct(){
        if (!class_exists('ZipArchive')){
            return false;
        }
    }

    ###########################
    # METHODS #################
    ###########################

    # create
    ###########################
    public function create($source,$destination){
        if (!extension_loaded('zip')){
            return false;
        }
        $zip = new ZipArchive();
        if (is_file($destination)){
            unlink($destination);
        }
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }
        if (is_string($source)){
            $source=array($source);
        }
        foreach($source as $item){
            if (is_dir($item) === true){
                $files = array_keys(iterator_to_array(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($item), RecursiveIteratorIterator::SELF_FIRST)));
                //$files[0]=dirname($files[1]);
                foreach ($files as $key=>$file){
                    # Ignore "." and ".." folders
                    if( in_array(substr($file, strrpos($file, DS)+1), array('.', '..')) ){
                        continue;
                    }
                    $file_short=utf8_decode(str_replace($item,'',$file));
                    if (is_dir($file) === true && $this->addSlash($file_short) != $item && $file_short != ''){
                        $zip->addEmptyDir($file_short);
                    }else if (is_file($file) === true){                       
                        $zip->addFromString($file_short, file_get_contents($file));
                    }
                }
            }
            else if (is_file($item) === true){
                $zip->addFromString($this->baseName($item), file_get_contents($item));
            }

        }
        $this->zipfile=$destination;
    }

    # unzip
    ###########################
    public function unzip($file,$destination){
        $zip = new ZipArchive() ;
        if ($zip->open($file) !== TRUE) {return false;} 
        $zip->extractTo($destination); 
        $zip->close(); 
        return true; 
    }
    
    # download
    ###########################
    public function download(){
        header('Content-type: application/zip');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($this->zipfile));
        header('Content-Disposition: attachment; filename="'.$this->baseName($this->zipfile).'"');
        readfile($this->zipfile);
    }

    # addSlash
    ###########################
    # add a slash if needed
    ## $string as string
    ## Return : string
    public function addSlash($string){
        if (substr($string,-1)!='/'&&!empty($string)){return $string.'/';}else{return $string;}
    }

    # baseName
    ###########################
    # basename without accent bug
    ## $file as string
    ## Return : string
    public function baseName($file=''){
        $array=explode(DIRECTORY_SEPARATOR,$file);
        if (is_array($array)){return end($array);}else{return $file;}
    }

}

?>