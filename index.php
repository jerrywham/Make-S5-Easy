<?php include 'prepend.php'; ?>
<!DOCTYPE html>
<html lang="<?php echo MS5E_LANG ?>">
<head>
    <meta charset=utf-8>
    <meta name=description content="A S5 slider maker">
    <title>Make-S5-Easy</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
    <?php if (is_file(MS5E_ROOT.'box.css')) :?>

    <link rel="stylesheet" href="box.css" type="text/css" media="screen" />
    <?php endif; ?>

</head>
<body>
    <div class="popup-wrapper popup noprint" id="help">
        <div class="popup-container">
            <div class="popup-content">
                <?php echo $help; ?>
            </div>
            <a class="popup-close" href="#closed">&times;</a>
        </div>
    </div>
    <header>
    <ul>
        <li class="btn active"><?php echo L_HOME;?></li>
        <li class="btn"><a href="<?php echo getRacine() ?>parameters.php"><?php echo L_PARAMETERS ?></a></li>
        <li class="btn"><a href="?preview" class="targetBlank"><?php echo L_PREVIEW;?></a></li>
        <li class="btn"><a href="?download"><?php echo L_DOWNLOAD;?></a></li>
        <li class="btn right danger"><a href="?reset" class="danger" title="<?php echo L_CONFIRM_RESET;?>"><?php echo L_RESET;?></a></li>
        <li class="btn right"><a href="#help" class="popup-link"><?php echo L_HELP;?></a></li>
    </ul>
    </header>
    <div class="main" id="main">
        <aside>
            <h2><?php echo L_SLIDES;?></h2>
            <?php echo $SLIDES;?>
            
        </aside>
        <section>
            <!-- 3 -->
            <form action="index.php" method="post" accept-charset="utf-8" name="editArea">
                <h2><?php echo L_SLIDE.' '.intval($slide_number);?></h2>
                <textarea name="editor" id="editor"><?php echo $slide_content;?></textarea>
                <h2><?php echo L_NOTES;?></h2>
                <textarea name="notes" id="notes"><?php echo $notes_content;?></textarea>
                <input type="hidden" name="number" value="<?php echo $slide_number;?>">
            </form>
        </section>
    </div>
    <footer>
        <!-- footer -->
        Make-S5-Easy by Cyril MAGUIRE 2017
    </footer>
    <script src="js/openwysiwyg_v1.4.7/scripts/wysiwyg.js"></script>
    <script src="js/openwysiwyg_v1.4.7/scripts/wysiwyg-settings.js"></script>
    <script>
        WYSIWYG.attach('editor', full);
        WYSIWYG.attach('notes', small);
        small.ImagePathToStrip = '<?php echo getRacine(); ?>medias/';
        full.ImagePathToStrip = '<?php echo getRacine(); ?>medias/';
    </script>
    <script src="js/utils.js"></script>
</body>
</html>