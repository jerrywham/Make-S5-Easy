<?php include 'prepend.php'; ?>
<!DOCTYPE html>
<html lang="<?php echo MS5E_LANG ?>">
<head>
    <meta charset=utf-8>
    <meta name=description content="A S5 slider maker">
    <title>Make-S5-Easy</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
</head>
<body>
    <header>
    <ul>
        <li class="btn"><a href="index.php"><?php echo L_HOME;?></a></li>
        <li class="btn active"><?php echo L_PARAMETERS ?></li>
    </ul>
    </header>
    <div class="main" id="main">
        <section class="params">
            <h1><?php echo L_PARAMETERS;?></h1>
            <form action="rec.php" method="post" accept-charset="utf-8">
                <fieldset>
                    <legend><?php echo L_ADMINISTRATIVE_PARAMETERS;?></legend>
                    <p>
                        <label for="LANG"><?php echo L_LANG;?></label>
                        <select name="LANG">
                            <?php foreach ($aLang as $key => $value) {
                                $value = str_replace('.php','',$value);
                            echo '<option value="'.$value.'"'.($value == $LANG ? ' selected="selected"' : '').'>'.$value.'</option>
                            ';    
                            } ?>
                        </select>
                    </p>
                    <p>
                        <label for="THEME"><?php echo L_THEME;?></label>
                        <select name="THEME">
                            <?php foreach ($aTHEMES as $key => $value) {
                                $value = str_replace('.php','',$value);
                            echo '<option value="'.$value.'"'.($value == $THEME ? ' selected="selected"' : '').'>'.$value.'</option>
                            ';    
                            } ?>
                        </select>
                    </p>
                    <p>
                        <label for="AUTHOR"><?php echo L_AUTHOR;?></label>
                        <input type="text" name="AUTHOR" value="<?php echo $AUTHOR;?>" placeholder="<?php echo L_AUTHOR;?>">
                    </p>
                    <p>
                        <label for="COMPANY"><?php echo L_COMPANY;?></label>
                        <input type="text" name="COMPANY" value="<?php echo $COMPANY;?>" placeholder="<?php echo L_COMPANY;?>">
                    </p>
                    <p>
                        <label for="COMPANY_LINK"><?php echo L_COMPANY_LINK;?></label>
                        <input type="text" name="COMPANY_LINK" value="<?php echo $COMPANY_LINK;?>" placeholder="http://www.">
                    </p>
                    <p>
                        <label for="TITLE"><?php echo L_GENERAL_TITLE;?><strong class="red" title="<?php echo L_REQUIRED;?>">*</strong></label>
                        <input type="text" name="TITLE" value="<?php echo $TITLE;?>" placeholder="<?php echo L_GENERAL_TITLE.' ('.L_REQUIRED.')';?>" required>
                    </p>
                    <p>
                        <label for="MAIN_TITLE"><?php echo L_MAIN_TITLE;?></label>
                        <input type="text" name="MAIN_TITLE" value="<?php echo $MAIN_TITLE;?>" placeholder="<?php echo L_MAIN_TITLE;?>">
                    </p>
                    <p>
                        <label for="MEETING_PLACE"><?php echo L_MEETING_PLACE;?></label>
                        <input type="text" name="MEETING_PLACE" value="<?php echo $MEETING_PLACE;?>" placeholder="<?php echo L_MEETING_PLACE;?>">
                    </p>
                    <p>
                        <label for="MEETING_DATE"><?php echo L_MEETING_DATE;?></label>
                        <input type="text" name="MEETING_DATE" value="<?php echo $MEETING_DATE;?>" placeholder="dd/mm/yyyy">
                    </p>
                </fieldset>
                <fieldset>
                    <legend><?php echo L_COLORS;?></legend>
                    <table>
                        <tr>
                            <td><?php echo L_HEADER_BACKGROUND_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenheader_background_color', style:'styleheader_background_color'} " id="styleheader_background_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $HEADER_BACKGROUND_COLOR;?>" id="hiddenheader_background_color" onChange="separate(this)" data-id="header_background_color"/>
                                
                                <input type="hidden" id="header_background_colorR" value="<?php echo extractRGBA($HEADER_BACKGROUND_COLOR,'R');?>"/>
                                <input type="hidden" id="header_background_colorG" value="<?php echo extractRGBA($HEADER_BACKGROUND_COLOR,'G');?>"/>
                                <input type="hidden" id="header_background_colorB" value="<?php echo extractRGBA($HEADER_BACKGROUND_COLOR,'B');?>"/>
                                <input type="hidden" id="header_background_colorT" value="<?php echo extractRGBA($HEADER_BACKGROUND_COLOR,'A');?>"/>                 
                                <input type="range"  id="header_background_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('header_background_color')">
                            </td>
                            <td>
                                <input type="text" name="HEADER_BACKGROUND_COLOR"  value="<?php echo $HEADER_BACKGROUND_COLOR;?>" id="header_background_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_HEADER_FONT_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenheader_font_color', style:'styleheader_font_color'} " id="styleheader_font_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $HEADER_FONT_COLOR;?>" id="hiddenheader_font_color" onChange="separate(this)" data-id="header_font_color"/>
                                
                                <input type="hidden" id="header_font_colorR" value="<?php echo extractRGBA($HEADER_FONT_COLOR,'R');?>"/>
                                <input type="hidden" id="header_font_colorG" value="<?php echo extractRGBA($HEADER_FONT_COLOR,'G');?>"/>
                                <input type="hidden" id="header_font_colorB" value="<?php echo extractRGBA($HEADER_FONT_COLOR,'B');?>"/>
                                <input type="hidden" id="header_font_colorT" value="<?php echo extractRGBA($HEADER_FONT_COLOR,'A');?>"/>                 
                                <input type="range"  id="header_font_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('header_font_color')">
                            </td>
                            <td>
                                <input type="text" name="HEADER_FONT_COLOR"  value="<?php echo $HEADER_FONT_COLOR;?>" id="header_font_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_HEADER_BORDER_BOTTOM_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenheader_border_bottom_color', style:'styleheader_border_bottom_color'} " id="styleheader_border_bottom_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $HEADER_BORDER_BOTTOM_COLOR;?>" id="hiddenheader_border_bottom_color" onChange="separate(this)" data-id="header_border_bottom_color"/>
                                
                                <input type="hidden" id="header_border_bottom_colorR" value="<?php echo extractRGBA($HEADER_BORDER_BOTTOM_COLOR,'R');?>"/>
                                <input type="hidden" id="header_border_bottom_colorG" value="<?php echo extractRGBA($HEADER_BORDER_BOTTOM_COLOR,'G');?>"/>
                                <input type="hidden" id="header_border_bottom_colorB" value="<?php echo extractRGBA($HEADER_BORDER_BOTTOM_COLOR,'B');?>"/>
                                <input type="hidden" id="header_border_bottom_colorT" value="<?php echo extractRGBA($HEADER_BORDER_BOTTOM_COLOR,'A');?>"/>                 
                                <input type="range"  id="header_border_bottom_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('header_border_bottom_color')">
                            </td>
                            <td>
                                <input type="text" name="HEADER_BORDER_BOTTOM_COLOR"  value="<?php echo $HEADER_BORDER_BOTTOM_COLOR;?>" id="header_border_bottom_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_MAIN_SECTION_BACKGROUND_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenmain_section_background_color', style:'stylemain_section_background_color'} " id="stylemain_section_background_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $MAIN_SECTION_BACKGROUND_COLOR;?>" id="hiddenmain_section_background_color" onChange="separate(this)" data-id="main_section_background_color"/>
                                
                                <input type="hidden" id="main_section_background_colorR" value="<?php echo extractRGBA($MAIN_SECTION_BACKGROUND_COLOR,'R');?>"/>
                                <input type="hidden" id="main_section_background_colorG" value="<?php echo extractRGBA($MAIN_SECTION_BACKGROUND_COLOR,'G');?>"/>
                                <input type="hidden" id="main_section_background_colorB" value="<?php echo extractRGBA($MAIN_SECTION_BACKGROUND_COLOR,'B');?>"/>
                                <input type="hidden" id="main_section_background_colorT" value="<?php echo extractRGBA($MAIN_SECTION_BACKGROUND_COLOR,'A');?>"/>                 
                                <input type="range"  id="main_section_background_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('main_section_background_color')">
                            </td>
                            <td>
                                <input type="text" name="MAIN_SECTION_BACKGROUND_COLOR"  value="<?php echo $MAIN_SECTION_BACKGROUND_COLOR;?>" id="main_section_background_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_MAIN_FONT_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenmain_font_color', style:'stylemain_font_color'} " id="stylemain_font_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $MAIN_FONT_COLOR;?>" id="hiddenmain_font_color" onChange="separate(this)" data-id="main_font_color"/>
                                
                                <input type="hidden" id="main_font_colorR" value="<?php echo extractRGBA($MAIN_FONT_COLOR,'R');?>"/>
                                <input type="hidden" id="main_font_colorG" value="<?php echo extractRGBA($MAIN_FONT_COLOR,'G');?>"/>
                                <input type="hidden" id="main_font_colorB" value="<?php echo extractRGBA($MAIN_FONT_COLOR,'B');?>"/>
                                <input type="hidden" id="main_font_colorT" value="<?php echo extractRGBA($MAIN_FONT_COLOR,'A');?>"/>                 
                                <input type="range"  id="main_font_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('main_font_color')">
                            </td>
                            <td>
                                <input type="text" name="MAIN_FONT_COLOR"  value="<?php echo $MAIN_FONT_COLOR;?>" id="main_font_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_FIRST_TITLE_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenfirst_title_color', style:'stylefirst_title_color'} " id="styleFIRST_TITLE_COLOR">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $FIRST_TITLE_COLOR;?>" id="hiddenfirst_title_color" onChange="separate(this)" data-id="first_title_color"/>
                                
                                <input type="hidden" id="first_title_colorR" value="<?php echo extractRGBA($FIRST_TITLE_COLOR,'R');?>"/>
                                <input type="hidden" id="first_title_colorG" value="<?php echo extractRGBA($FIRST_TITLE_COLOR,'G');?>"/>
                                <input type="hidden" id="first_title_colorB" value="<?php echo extractRGBA($FIRST_TITLE_COLOR,'B');?>"/>
                                <input type="hidden" id="first_title_colorT" value="<?php echo extractRGBA($FIRST_TITLE_COLOR,'A');?>"/>                 
                                <input type="range"  id="first_title_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('first_title_color')">
                            </td>
                            <td>
                                <input type="text" name="FIRST_TITLE_COLOR"  value="<?php echo $FIRST_TITLE_COLOR;?>" id="first_title_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_MAIN_TITLE_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenmain_title_color', style:'stylemain_title_color'} " id="stylemain_title_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $MAIN_TITLE_COLOR;?>" id="hiddenmain_title_color" onChange="separate(this)" data-id="main_title_color"/>
                                
                                <input type="hidden" id="main_title_colorR" value="<?php echo extractRGBA($MAIN_TITLE_COLOR,'R');?>"/>
                                <input type="hidden" id="main_title_colorG" value="<?php echo extractRGBA($MAIN_TITLE_COLOR,'G');?>"/>
                                <input type="hidden" id="main_title_colorB" value="<?php echo extractRGBA($MAIN_TITLE_COLOR,'B');?>"/>
                                <input type="hidden" id="main_title_colorT" value="<?php echo extractRGBA($MAIN_TITLE_COLOR,'A');?>"/>                 
                                <input type="range"  id="main_title_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('main_title_color')">
                            </td>
                            <td>
                                <input type="text" name="MAIN_TITLE_COLOR"  value="<?php echo $MAIN_TITLE_COLOR;?>" id="main_title_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_MAIN_TITLE_BACKGROUND_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenmain_title_background_color', style:'stylemain_title_background_color'} " id="stylemain_title_background_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $MAIN_TITLE_BACKGROUND_COLOR;?>" id="hiddenmain_title_background_color" onChange="separate(this)" data-id="main_title_background_color"/>
                                
                                <input type="hidden" id="main_title_background_colorR" value="<?php echo extractRGBA($MAIN_TITLE_BACKGROUND_COLOR,'R');?>"/>
                                <input type="hidden" id="main_title_background_colorG" value="<?php echo extractRGBA($MAIN_TITLE_BACKGROUND_COLOR,'G');?>"/>
                                <input type="hidden" id="main_title_background_colorB" value="<?php echo extractRGBA($MAIN_TITLE_BACKGROUND_COLOR,'B');?>"/>
                                <input type="hidden" id="main_title_background_colorT" value="<?php echo extractRGBA($MAIN_TITLE_BACKGROUND_COLOR,'A');?>"/>                 
                                <input type="range"  id="main_title_background_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('main_title_background_color')">
                            </td>
                            <td>
                                <input type="text" name="MAIN_TITLE_BACKGROUND_COLOR"  value="<?php echo $MAIN_TITLE_BACKGROUND_COLOR;?>" id="main_title_background_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CURRENT_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencurrent_color', style:'stylecurrent_color'} " id="stylecurrent_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CURRENT_COLOR;?>" id="hiddencurrent_color" onChange="separate(this)" data-id="current_color"/>
                                
                                <input type="hidden" id="current_colorR" value="<?php echo extractRGBA($CURRENT_COLOR,'R');?>"/>
                                <input type="hidden" id="current_colorG" value="<?php echo extractRGBA($CURRENT_COLOR,'G');?>"/>
                                <input type="hidden" id="current_colorB" value="<?php echo extractRGBA($CURRENT_COLOR,'B');?>"/>
                                <input type="hidden" id="current_colorT" value="<?php echo extractRGBA($CURRENT_COLOR,'A');?>"/>                 
                                <input type="range"  id="current_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('current_color')">
                            </td>
                            <td>
                                <input type="text" name="CURRENT_COLOR"  value="<?php echo $CURRENT_COLOR;?>" id="current_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_LINKS_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenlinks_color', style:'stylelinks_color'} " id="stylelinks_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $LINKS_COLOR;?>" id="hiddenlinks_color" onChange="separate(this)" data-id="links_color"/>
                                
                                <input type="hidden" id="links_colorR" value="<?php echo extractRGBA($LINKS_COLOR,'R');?>"/>
                                <input type="hidden" id="links_colorG" value="<?php echo extractRGBA($LINKS_COLOR,'G');?>"/>
                                <input type="hidden" id="links_colorB" value="<?php echo extractRGBA($LINKS_COLOR,'B');?>"/>
                                <input type="hidden" id="links_colorT" value="<?php echo extractRGBA($LINKS_COLOR,'A');?>"/>                 
                                <input type="range"  id="links_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('links_color')">
                            </td>
                            <td>
                                <input type="text" name="LINKS_COLOR"  value="<?php echo $LINKS_COLOR;?>" id="links_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_FOOTER_BACKGROUND_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenfooter_background_color', style:'stylefooter_background_color'} " id="stylefooter_background_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $FOOTER_BACKGROUND_COLOR;?>" id="hiddenfooter_background_color" onChange="separate(this)" data-id="footer_background_color"/>
                                
                                <input type="hidden" id="footer_background_colorR" value="<?php echo extractRGBA($FOOTER_BACKGROUND_COLOR,'R');?>"/>
                                <input type="hidden" id="footer_background_colorG" value="<?php echo extractRGBA($FOOTER_BACKGROUND_COLOR,'G');?>"/>
                                <input type="hidden" id="footer_background_colorB" value="<?php echo extractRGBA($FOOTER_BACKGROUND_COLOR,'B');?>"/>
                                <input type="hidden" id="footer_background_colorT" value="<?php echo extractRGBA($FOOTER_BACKGROUND_COLOR,'A');?>"/>                 
                                <input type="range"  id="footer_background_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('footer_background_color')">
                            </td>
                            <td>
                                <input type="text" name="FOOTER_BACKGROUND_COLOR"  value="<?php echo $FOOTER_BACKGROUND_COLOR;?>" id="footer_background_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_FOOTER_FONT_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenfooter_font_color', style:'stylefooter_font_color'} " id="stylefooter_font_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $FOOTER_FONT_COLOR;?>" id="hiddenfooter_font_color" onChange="separate(this)" data-id="footer_font_color"/>
                                
                                <input type="hidden" id="footer_font_colorR" value="<?php echo extractRGBA($FOOTER_FONT_COLOR,'R');?>"/>
                                <input type="hidden" id="footer_font_colorG" value="<?php echo extractRGBA($FOOTER_FONT_COLOR,'G');?>"/>
                                <input type="hidden" id="footer_font_colorB" value="<?php echo extractRGBA($FOOTER_FONT_COLOR,'B');?>"/>
                                <input type="hidden" id="footer_font_colorT" value="<?php echo extractRGBA($FOOTER_FONT_COLOR,'A');?>"/>                 
                                <input type="range"  id="footer_font_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('footer_font_color')">
                            </td>
                            <td>
                                <input type="text" name="FOOTER_FONT_COLOR"  value="<?php echo $FOOTER_FONT_COLOR;?>" id="footer_font_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_FOOTER_BORDER_TOP_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenfooter_border_top_color', style:'stylefooter_border_top_color'} " id="stylefooter_border_top_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $FOOTER_BORDER_TOP_COLOR;?>" id="hiddenfooter_border_top_color" onChange="separate(this)" data-id="footer_border_top_color"/>
                                
                                <input type="hidden" id="footer_border_top_colorR" value="<?php echo extractRGBA($FOOTER_BORDER_TOP_COLOR,'R');?>"/>
                                <input type="hidden" id="footer_border_top_colorG" value="<?php echo extractRGBA($FOOTER_BORDER_TOP_COLOR,'G');?>"/>
                                <input type="hidden" id="footer_border_top_colorB" value="<?php echo extractRGBA($FOOTER_BORDER_TOP_COLOR,'B');?>"/>
                                <input type="hidden" id="footer_border_top_colorT" value="<?php echo extractRGBA($FOOTER_BORDER_TOP_COLOR,'A');?>"/>                 
                                <input type="range"  id="footer_border_top_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('footer_border_top_color')">
                            </td>
                            <td>
                                <input type="text" name="FOOTER_BORDER_TOP_COLOR"  value="<?php echo $FOOTER_BORDER_TOP_COLOR;?>" id="footer_border_top_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CONTROLS_NAVLIST_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencontrols_navlist_color', style:'stylecontrols_navlist_color'} " id="stylecontrols_navlist_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CONTROLS_NAVLIST_COLOR;?>" id="hiddencontrols_navlist_color" onChange="separate(this)" data-id="controls_navlist_color"/>
                                
                                <input type="hidden" id="controls_navlist_colorR" value="<?php echo extractRGBA($CONTROLS_NAVLIST_COLOR,'R');?>"/>
                                <input type="hidden" id="controls_navlist_colorG" value="<?php echo extractRGBA($CONTROLS_NAVLIST_COLOR,'G');?>"/>
                                <input type="hidden" id="controls_navlist_colorB" value="<?php echo extractRGBA($CONTROLS_NAVLIST_COLOR,'B');?>"/>
                                <input type="hidden" id="controls_navlist_colorT" value="<?php echo extractRGBA($CONTROLS_NAVLIST_COLOR,'A');?>"/>                 
                                <input type="range"  id="controls_navlist_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('controls_navlist_color')">
                            </td>
                            <td>
                                <input type="text" name="CONTROLS_NAVLIST_COLOR"  value="<?php echo $CONTROLS_NAVLIST_COLOR;?>" id="controls_navlist_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CONTROLS_NAVLIST_BACKGROUND_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencontrols_navlist_background_color', style:'stylecontrols_navlist_background_color'} " id="stylecontrols_navlist_background_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CONTROLS_NAVLIST_BACKGROUND_COLOR;?>" id="hiddencontrols_navlist_background_color" onChange="separate(this)" data-id="controls_navlist_background_color"/>
                                
                                <input type="hidden" id="controls_navlist_background_colorR" value="<?php echo extractRGBA($CONTROLS_NAVLIST_BACKGROUND_COLOR,'R');?>"/>
                                <input type="hidden" id="controls_navlist_background_colorG" value="<?php echo extractRGBA($CONTROLS_NAVLIST_BACKGROUND_COLOR,'G');?>"/>
                                <input type="hidden" id="controls_navlist_background_colorB" value="<?php echo extractRGBA($CONTROLS_NAVLIST_BACKGROUND_COLOR,'B');?>"/>
                                <input type="hidden" id="controls_navlist_background_colorT" value="<?php echo extractRGBA($CONTROLS_NAVLIST_BACKGROUND_COLOR,'A');?>"/>                 
                                <input type="range"  id="controls_navlist_background_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('controls_navlist_background_color')">
                            </td>
                            <td>
                                <input type="text" name="CONTROLS_NAVLIST_BACKGROUND_COLOR"  value="<?php echo $CONTROLS_NAVLIST_BACKGROUND_COLOR;?>" id="controls_navlist_background_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CONTROLS_NAVLIST_LINKS_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencontrols_navlist_links_color', style:'stylecontrols_navlist_links_color'} " id="stylecontrols_navlist_links_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CONTROLS_NAVLIST_LINKS_COLOR;?>" id="hiddencontrols_navlist_links_color" onChange="separate(this)" data-id="controls_navlist_links_color"/>
                                
                                <input type="hidden" id="controls_navlist_links_colorR" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_COLOR,'R');?>"/>
                                <input type="hidden" id="controls_navlist_links_colorG" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_COLOR,'G');?>"/>
                                <input type="hidden" id="controls_navlist_links_colorB" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_COLOR,'B');?>"/>
                                <input type="hidden" id="controls_navlist_links_colorT" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_COLOR,'A');?>"/>                 
                                <input type="range"  id="controls_navlist_links_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('controls_navlist_links_color')">
                            </td>
                            <td>
                                <input type="text" name="CONTROLS_NAVLIST_LINKS_COLOR"  value="<?php echo $CONTROLS_NAVLIST_LINKS_COLOR;?>" id="controls_navlist_links_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencontrols_navlist_links_background_color', style:'stylecontrols_navlist_links_background_color'} " id="stylecontrols_navlist_links_background_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR;?>" id="hiddencontrols_navlist_links_background_color" onChange="separate(this)" data-id="controls_navlist_links_background_color"/>
                                
                                <input type="hidden" id="controls_navlist_links_background_colorR" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR,'R');?>"/>
                                <input type="hidden" id="controls_navlist_links_background_colorG" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR,'G');?>"/>
                                <input type="hidden" id="controls_navlist_links_background_colorB" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR,'B');?>"/>
                                <input type="hidden" id="controls_navlist_links_background_colorT" value="<?php echo extractRGBA($CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR,'A');?>"/>                 
                                <input type="range"  id="controls_navlist_links_background_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('controls_navlist_links_background_color')">
                            </td>
                            <td>
                                <input type="text" name="CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR"  value="<?php echo $CONTROLS_NAVLIST_LINKS_BACKGROUND_COLOR;?>" id="controls_navlist_links_background_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CONTROLS_ACTIVE_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencontrols_active_color', style:'stylecontrols_active_color'} " id="stylecontrols_active_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CONTROLS_ACTIVE_COLOR;?>" id="hiddencontrols_active_color" onChange="separate(this)" data-id="controls_active_color"/>
                                
                                <input type="hidden" id="controls_active_colorR" value="<?php echo extractRGBA($CONTROLS_ACTIVE_COLOR,'R');?>"/>
                                <input type="hidden" id="controls_active_colorG" value="<?php echo extractRGBA($CONTROLS_ACTIVE_COLOR,'G');?>"/>
                                <input type="hidden" id="controls_active_colorB" value="<?php echo extractRGBA($CONTROLS_ACTIVE_COLOR,'B');?>"/>
                                <input type="hidden" id="controls_active_colorT" value="<?php echo extractRGBA($CONTROLS_ACTIVE_COLOR,'A');?>"/>                 
                                <input type="range"  id="controls_active_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('controls_active_color')">
                            </td>
                            <td>
                                <input type="text" name="CONTROLS_ACTIVE_COLOR"  value="<?php echo $CONTROLS_ACTIVE_COLOR;?>" id="controls_active_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CONTROLS_FOCUS_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencontrols_focus_color', style:'stylecontrols_focus_color'} " id="stylecontrols_focus_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CONTROLS_FOCUS_COLOR;?>" id="hiddencontrols_focus_color" onChange="separate(this)" data-id="controls_focus_color"/>
                                
                                <input type="hidden" id="controls_focus_colorR" value="<?php echo extractRGBA($CONTROLS_FOCUS_COLOR,'R');?>"/>
                                <input type="hidden" id="controls_focus_colorG" value="<?php echo extractRGBA($CONTROLS_FOCUS_COLOR,'G');?>"/>
                                <input type="hidden" id="controls_focus_colorB" value="<?php echo extractRGBA($CONTROLS_FOCUS_COLOR,'B');?>"/>
                                <input type="hidden" id="controls_focus_colorT" value="<?php echo extractRGBA($CONTROLS_FOCUS_COLOR,'A');?>"/>                 
                                <input type="range"  id="controls_focus_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('controls_focus_color')">
                            </td>
                            <td>
                                <input type="text" name="CONTROLS_FOCUS_COLOR"  value="<?php echo $CONTROLS_FOCUS_COLOR;?>" id="controls_focus_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_SLIDESNUMBERS_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenslidesnumbers_color', style:'styleslidesnumbers_color'} " id="styleslidesnumbers_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $SLIDESNUMBERS_COLOR;?>" id="hiddenslidesnumbers_color" onChange="separate(this)" data-id="slidesnumbers_color"/>
                                
                                <input type="hidden" id="slidesnumbers_colorR" value="<?php echo extractRGBA($SLIDESNUMBERS_COLOR,'R');?>"/>
                                <input type="hidden" id="slidesnumbers_colorG" value="<?php echo extractRGBA($SLIDESNUMBERS_COLOR,'G');?>"/>
                                <input type="hidden" id="slidesnumbers_colorB" value="<?php echo extractRGBA($SLIDESNUMBERS_COLOR,'B');?>"/>
                                <input type="hidden" id="slidesnumbers_colorT" value="<?php echo extractRGBA($SLIDESNUMBERS_COLOR,'A');?>"/>                 
                                <input type="range"  id="slidesnumbers_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('slidesnumbers_color')">
                            </td>
                            <td>
                                <input type="text" name="SLIDESNUMBERS_COLOR"  value="<?php echo $SLIDESNUMBERS_COLOR;?>" id="slidesnumbers_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_CODE_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddencode_color', style:'stylecode_color'} " id="stylecode_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $CODE_COLOR;?>" id="hiddencode_color" onChange="separate(this)" data-id="code_color"/>
                                
                                <input type="hidden" id="code_colorR" value="<?php echo extractRGBA($CODE_COLOR,'R');?>"/>
                                <input type="hidden" id="code_colorG" value="<?php echo extractRGBA($CODE_COLOR,'G');?>"/>
                                <input type="hidden" id="code_colorB" value="<?php echo extractRGBA($CODE_COLOR,'B');?>"/>
                                <input type="hidden" id="code_colorT" value="<?php echo extractRGBA($CODE_COLOR,'A');?>"/>                 
                                <input type="range"  id="code_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('code_color')">
                            </td>
                            <td>
                                <input type="text" name="CODE_COLOR"  value="<?php echo $CODE_COLOR;?>" id="code_colorRGBvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo L_STRONG_COLOR;?></td>
                            <td>
                                <button class="jscolor {valueElement:'hiddenstrong_color', style:'stylestrong_color'} " id="stylestrong_color">&nbsp;</button>
                            </td>
                            <td>
                                <input type="hidden" value="<?php echo $STRONG_COLOR;?>" id="hiddenstrong_color" onChange="separate(this)" data-id="strong_color"/>
                                
                                <input type="hidden" id="strong_colorR" value="<?php echo extractRGBA($STRONG_COLOR,'R');?>"/>
                                <input type="hidden" id="strong_colorG" value="<?php echo extractRGBA($STRONG_COLOR,'G');?>"/>
                                <input type="hidden" id="strong_colorB" value="<?php echo extractRGBA($STRONG_COLOR,'B');?>"/>
                                <input type="hidden" id="strong_colorT" value="<?php echo extractRGBA($STRONG_COLOR,'A');?>"/>                 
                                <input type="range"  id="strong_colortransparency"  value="1" min="0" max="1" step="0.1" onChange="updateTransp('strong_color')">
                            </td>
                            <td>
                                <input type="text" name="STRONG_COLOR"  value="<?php echo $STRONG_COLOR;?>" id="strong_colorrgbvalue" onChange="update(this)"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <p class="clear">
                    <input type="submit" value="<?php echo L_REC;?>">
                </p>
            </form>
        </section>
    </div>
    <footer>
        <!-- footer -->
        <?php echo MS5E_FOOTER;?>

    </footer>
    <script src="js/jscolor.js"></script>
    <script>
        
        function hexToRgb(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            result =  result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
            return result;

        }
        function separate(obj){
            rgb                     =hexToRgb(obj.value);
            id                      =attr(obj,"data-id")
            first("#"+id+"R").value =rgb['r'];
            first("#"+id+"G").value =rgb['g'];
            first("#"+id+"B").value =rgb['b'];
            update(id);
        }
        function update(id){
            val ='rgba('+first("#"+id+"R").value+','+first("#"+id+"G").value+','+first("#"+id+"B").value+','+first("#"+id+"T").value+')';
            first("#"+id+"RGBvalue").value=val;
        }
        function updateTransp(id){
            obj         =first('#'+id+'transparency');
            prev        =first('#'+id+'T');
            prev.value  =obj.value;
            update(id);
        }

        function hash() {
            // from http://www.italic.fr/generer-un-password-en-javascript/
            var ok   = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ@-!_';
            var hash = '';
            longueur = 512;
            for(i=0;i<longueur;i++){
                var wpos = Math.round(Math.random()*ok.length);
                hash+=ok.substring(wpos,wpos+1);
            }
            return hash;
        }

        function first(tag){return document.querySelector(tag);}
        function attr(obj,attr,value){
            obj=_(obj);if (!obj){return false}
            if (!obj.length){
                if (value){obj.setAttribute(attr,value);}
                else{
                    if (obj.getAttribute){return obj.getAttribute(attr);}
                    else{return false;}
                }
            }
            if (obj.length){
                if (value){
                    [].forEach.call(obj, function(el) {el.setAttribute(attr,value);});
                }else{return obj[0].getAttribute(attr);}
            }
        }
        function _(obj,single){
            if (typeof obj=='string'){
                if (single){return first(obj);}
                else{return all(obj);}
            }else{
                if (single && obj[0]){return obj[0];}
            }
            return obj;
        }
    </script>
</body>
</html>