<?php
if (empty($_POST)) {
    header('location:index.php');
    exit();
}
include_once 'prepend.php';
$footer = '<div id="footer">
<h1>{{MAIN_TITLE}}</h1>
<h2>{{AUTHOR}}, {{MEETING_PLACE}} {{DATE}}</h2>
</div>';
$slide = '
<!-- slide --> <div class="slide">
{{SLIDE_CONTENT}}

{{SLIDE_NOTES_CONTENT}}
</div> <!-- /slide -->
';
$notes = '
<!-- notes --> <div class="notes">
{{SLIDE_NOTES}}

</div> <!-- /notes -->
';
if (is_file(MS5E_TEMPLATE.'index.slidetpl')) {
    $template = file_get_contents(MS5E_TEMPLATE.'index.slidetpl');
} else {
    throw new Exception("Error : Missing template", 1);
}


// SLIDES CONTENT
if (isset($_POST['editor']) && !empty($_POST['editor'])) {
    $content = $_POST['editor'];
    $slide = str_replace('{{SLIDE_CONTENT}}',$content,$slide);
    if (isset($_POST['notes']) && !empty($_POST['notes'])) {
        $notes_content = $_POST['notes'];
        $notes_final = str_replace('{{SLIDE_NOTES}}',$notes_content,$notes);
        $slide = str_replace('{{SLIDE_NOTES_CONTENT}}',$notes_final,$slide);
    }
    $slide = str_replace('><','>'."\n".'<',$slide);
    $final = str_replace('{{SLIDE_CONTENT}}',$content,$slide);
    $final = str_replace('{{SLIDE_NOTES_CONTENT}}','',$slide);
    file_put_contents(MS5E_SLIDES_PATH.'content'.DS.$slide_number.'.txt',$final);
}

// PARAMS AND COLORS
if(!isset($_POST['editor']) ) {
    $colors = '';
    $params = '';
    $pretty = file_get_contents(MS5E_TEMPLATE.'themes'.DS.$THEME.DS.'pretty.csstpl');
    $box = file_get_contents(MS5E_TEMPLATE.'themes'.DS.$THEME.DS.'box.csstpl');
    foreach ($_POST as $key => $value) {
        if (isset($$key)) {
            if (strpos($key,'_COLOR') !== false) {
                if (!empty($value)) {
                    $colors .= '$'.$key.' = \''.$value.'\';'."\n";
                    $pretty = str_replace('$'.$key,$value,$pretty);
                    $box = str_replace('$'.$key,$value,$box);
                }
            } else {
                $params .= '$'.$key.' = \''.$value.'\';'."\n";
                $$key = $value;
            }
        }
    }

    file_put_contents(MS5E_PRESENTATION_PATH.'ui'.DS.$THEME.DS.'pretty.css',$pretty);
    file_put_contents(MS5E_ROOT.'box.css',$box);

    if (!empty($colors)) {
        $colors = '<?php '."\n".$colors;
        file_put_contents(MS5E_SLIDES_PATH.'colors.php',$colors);
    }
    if (!empty($params)) {
        $params = '<?php '."\n".$params;
        file_put_contents(MS5E_SLIDES_PATH.'params.php',$params);
    }   
    $footer = str_replace(
            array('{{MAIN_TITLE}}', '{{AUTHOR}}','{{MEETING_PLACE}}', '{{DATE}}'),
            array($MAIN_TITLE, $AUTHOR, $MEETING_PLACE, $MEETING_DATE),
            $footer
    );
    file_put_contents(MS5E_SLIDES_PATH.'footer.php',$footer);
    recSlider($TITLE,$LANG,$AUTHOR,$COMPANY,$MAIN_TITLE,$MEETING_PLACE,$COMPANY_LINK);
}
