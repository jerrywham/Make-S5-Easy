<?php 
if (empty($_GET)) {
    header('location:index.php');
    exit();
} 
include_once 'prepend.php';

//Duplicate
if (isset($_GET['duplicate']) && is_file(MS5E_SLIDES_PATH.'content'.DS.$_GET['duplicate'].'.txt') ) {
    $nextId = nextId(MS5E_SLIDES_PATH.'content');
    copy(MS5E_SLIDES_PATH.'content'.DS.$_GET['duplicate'].'.txt',MS5E_SLIDES_PATH.'content'.DS.$nextId.'.txt');    
}
//Rename
if (isset($_GET['rename']) && is_file(MS5E_SLIDES_PATH.'content'.DS.$_GET['old'].'.txt') ) {
    
    $new = str_pad(intval($_GET['rename']),4, '0', STR_PAD_LEFT);
    $nextId = nextId(MS5E_SLIDES_PATH.'content');
    if (intval($new) > intval($nextId) || intval($new) <= 0) {
        $new = $nextId;
    }
    if (is_file(MS5E_SLIDES_PATH.'content'.DS.$_GET['old'].'.txt')) {
        rename(MS5E_SLIDES_PATH.'content'.DS.$_GET['old'].'.txt',MS5E_SLIDES_PATH.'content'.DS.'tmp.txt');
    }
    if (is_file(MS5E_SLIDES_PATH.'content'.DS.$new.'.txt')) {
        rename(MS5E_SLIDES_PATH.'content'.DS.$new.'.txt',MS5E_SLIDES_PATH.'content'.DS.$_GET['old'].'.txt');
    }
    if (is_file(MS5E_SLIDES_PATH.'content'.DS.'tmp.txt')) {
        rename(MS5E_SLIDES_PATH.'content'.DS.'tmp.txt',MS5E_SLIDES_PATH.'content'.DS.$new.'.txt');
    }

}
//Remove
if (isset($_GET['remove']) && is_file(MS5E_SLIDES_PATH.'content'.DS.$_GET['remove'].'.txt') ) {
    unlink(MS5E_SLIDES_PATH.'content'.DS.$_GET['remove'].'.txt');
    reIndexSlides();
}
//Edit
if (isset($_GET['edit']) && is_file(MS5E_SLIDES_PATH.'content'.DS.$_GET['edit'].'.txt') ) {
    $content = file_get_contents(MS5E_SLIDES_PATH.'content'.DS.$_GET['edit'].'.txt');
    $content = trim(str_replace(
        array(
            '<!-- slide --> <div class="slide">',
            '<!-- notes --> <div class="notes">',
            '</div> <!-- /notes -->'
        ),
        array(
            'START_SLIDE',
            'START_NOTES',
            'END_NOTES'
        ),$content));
    $slide_number = $_GET['edit'];
    $slide_content = trim(mb_substr($content,11,mb_strpos($content,'START_NOTES',0,'UTF-8')-11,'UTF-8'));
    $notes_content = trim(mb_substr($content,mb_strpos($content,'START_NOTES',0,'UTF-8')+11,null,'UTF-8'));
    $notes_content = trim(mb_substr($notes_content,0,mb_strpos($notes_content,'END_NOTES',0,'UTF-8'),'UTF-8'));
}
//Preview
if(isset($_GET['preview'])) {
    recSlider($TITLE,$LANG,$AUTHOR,$COMPANY,$MAIN_TITLE,$MEETING_PLACE,$COMPANY_LINK);
    $name = title2filename($TITLE);
    $racine = getRacine().'data/presentation/';
    header('Location:'.$racine.$name.'.html');
    exit();
}
//Reset 
if (isset($_GET['reset'])){
    $name = title2filename($TITLE);
    $files = initCache(MS5E_SLIDES_PATH.'content','files',true);    
    if (is_file(MS5E_PRESENTATION_PATH.$name.'.html')) {
        unlink(MS5E_ROOT.'box.css');
        unlink(MS5E_PRESENTATION_PATH.$name.'.html');
        unlink(MS5E_TMP_PATH.$name.'.zip');
        file_put_contents(MS5E_PRESENTATION_PATH.'ui'.DS.$THEME.DS.'pretty.css','<?php'."\n");
        file_put_contents(MS5E_SLIDES_PATH.'colors.php','<?php'."\n");
        file_put_contents(MS5E_SLIDES_PATH.'params.php','<?php'."\n");
        file_put_contents(MS5E_SLIDES_PATH.'footer.php','');
        foreach ($files as $key => $file) {
            if (is_file(MS5E_SLIDES_PATH.'content'.DS.$file)) {
                unlink(MS5E_SLIDES_PATH.'content'.DS.$file);
            }
        }
    }
}
//Download zipFile
if (isset($_GET['download'])) {
    $zip=new zip;
    $zip->create(MS5E_PRESENTATION_PATH,MS5E_TMP_PATH.title2filename($TITLE).'.zip');
    $zip->download();
}