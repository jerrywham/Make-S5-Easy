var url = window.location.href;
var params = url;
var reg = new RegExp('index\.php.*');
url = url.replace(reg,'',url);
params = params.replace(url,'');
params = params.replace('index.php?','');

var slide = null;

function doAction(className,action) {
    var elemWithClass = document.querySelectorAll('.'+className);
    var nbElemWithClass = elemWithClass.length;
    for (var i = 0; i < nbElemWithClass; i++) {
        elemWithClass[i].addEventListener('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            slide = this.id.replace(action+'-','');
            var msg = this.title;
            if (typeof slide !== undefined) {
                switch(action) {
                    case 'remove':
                    case 'duplicate':
                        if (confirm(msg+' '+slide)) {
                            window.location.href = url+'index.php?'+action+'='+slide;
                        }
                        break;
                    case 'rename':
                        var newId = null;
                        if (newId = prompt(msg)) {
                            window.location.href = url+'index.php?'+action+'='+newId+'&old='+slide;
                        }
                        break;
                    case 'edit':
                        window.location.href = url+'index.php?'+action+'='+slide;
                        break;
                    case 'add':
                        document.getElementById('editor').value = "\n";
                        document.forms[0].submit();
                        break;
                }
            }
        });
    }
}

var xMousePosition = 0;
var yMousePosition = 0;
document.onmousemove = function(e)
{
    xMousePosition = e.clientX + window.pageXOffset;
    yMousePosition = e.clientY + window.pageYOffset;
};

function hasClass(object, className) {      
    if (!object.className || object.className === undefined) return false;
    return (object.className.search('(^|\\s)' + className + '(\\s|$)') != -1);
}

function removeClass(object,className) {
    if (!object || !hasClass(object,className)) return;
    object.className = object.className.replace(new RegExp('(^|\\s)'+className+'(\\s|$)'), RegExp.$1+RegExp.$2);
}

function addClass(object,className) {
    if (!object || hasClass(object, className)) return;
    if (object.className) {
        object.className += ' '+className;
    } else {
        object.className = className;
    }
}

var menus = document.querySelectorAll('.contextmenu');
var nbMenus = menus.length;
for (var i = 0; i < nbMenus; i++) {
    d = menus[i];        
    if (hasClass(d.firstElementChild,'show')) {
        removeClass(d.firstElementChild,'show');
    }
    addClass(d.firstElementChild,'hide');
}
function monmenu(element) {
    d = element.firstElementChild;
    if (d === undefined) return false;
    removeClass(d,'hide');
    addClass(d,'show');
    d.style.left = xMousePosition + "px";
    d.style.top = yMousePosition + "px"; 
    d.onmouseover = function(e) { this.style.cursor = 'pointer'; } 
    d.onmouseleave = function(e) { removeClass(d,'show');addClass(d,'hide');  } 
    d.onclick = function(e) { removeClass(d,'show');addClass(d,'hide');  }
    document.body.onclick = function(e) { removeClass(d,'show');addClass(d,'hide'); }
    return false;
}

var targetBlank = document.querySelector('a.targetBlank').addEventListener('click', function(evt) { 
    evt.preventDefault(); 
    evt.returnValue = false; 
    openNew(this.href);});

function openNew(url) {
    var otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = url;
}

function danger() {
    var dangerElements = document.querySelectorAll('.danger');
    var dangerLength = dangerElements.length;
    for (var i = 0; i < dangerLength; i++) {
        dangerElements[i].addEventListener('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var msg = this.title;
            if (!confirm(msg)) { 
                e.returnValue = false;
            } else {
                window.location = this.href;
            }
        });
    }
}

doAction('remove','remove');
doAction('duplicate','duplicate');
doAction('rename','rename');
doAction('editor','edit');
doAction('add','add');
danger();

var side = document.querySelector('aside');
var h = window.innerHeight-(50+40);// height of main - (height of header - height of footer)
side.style="height:"+h+"px;";