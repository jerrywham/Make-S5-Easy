<?php 
require_once '../../../prepend.php';

if(!defined('MS5E_ROOT')) exit;
?><!DOCTYPE html>

<html lang="<?php echo MS5E_LANG ?>">
	<head>
		<title>openWYSIWYG | <?php echo L_CREATE_OR_MODIFY_TABLE;?></title>

		<style type="text/css">
			body, td {
				font-family: arial, verdana, helvetica; 
				font-size: 11px;
			}
			
			select, input, button {
				font-size: 10px;
			}
			
			.table-settings {
				background-color: #F7F7F7; 
				border: 2px solid #FFFFFF; 
				padding: 5px;			
			}
		</style>

	<script type="text/javascript" src="<?php echo getUrl('js/openwysiwyg_v1.4.7/scripts/wysiwyg-popup.js');?>"></script>
	<script type="text/javascript" src="<?php echo getUrl('js/openwysiwyg_v1.4.7/scripts/wysiwyg-color.js');?>"></script>
		
	<script type="text/javascript">
	var n = WYSIWYG_Popup.getParam('wysiwyg');
		
	// add stylesheet file
	if(n) document.write('<link rel="stylesheet" type="text/css" href="' + WYSIWYG.config[n].CSSFile +'">\n');
	
	/* ---------------------------------------------------------------------- *\
	  Function    : buildTable()
	  Description : Builds a table and inserts it into the WYSIWYG.
	\* ---------------------------------------------------------------------- */
	function buildTable() {

		var n = WYSIWYG_Popup.getParam('wysiwyg');

		// get selection and range
		var sel = WYSIWYG.getSelection(n);
		var range = WYSIWYG.getRange(sel);

		// the current tag of range
		var tab = WYSIWYG.findParent("table", range);
					
		// if no table is defined then return
		if(tab == null) {
			var WYSIWYG_Table = window.opener.WYSIWYG_Table;
			var doc = WYSIWYG.getEditorWindow(n).document;
			// create a table object
			var table = doc.createElement("TABLE");
		} else {
			var WYSIWYG_Table = window.opener.WYSIWYG_Table;
			var doc = WYSIWYG.getEditorWindow(n).document;
			var table = tab;
		}
		var formNbCols = document.getElementById("cols").value;
		formNbCols = (formNbCols < 1) ? 1 : formNbCols;
		var formNbRows = document.getElementById("rows").value;
		formNbRows = (formNbRows < 1) ? 1 : formNbRows;
		var formAlign = document.getElementById("alignment").value;
		var formBorderStyle = document.getElementById("borderstyle").value;
		var formBorderColor = document.getElementById("bordercolor").value;
		var formBackgroundColor = document.getElementById("backgroundcolor").value;

		if(tab == null) {
			// set cols and rows
			WYSIWYG_Core.setAttribute(table, "tmpcols", formNbCols);
			WYSIWYG_Core.setAttribute(table, "tmprows", formNbRows);
		}
			// alignment
			if(formAlign != "") 
				WYSIWYG_Core.setAttribute(table, "align", formAlign);
			
			// style attributes
			var style = "";
			var styleWithoutWidth = "";
			// padding
			style += "padding:" + document.getElementById("padding").value + "px;";
			styleWithoutWidth += "padding:" + document.getElementById("padding").value + "px;";
			// width
			style += "width:" + document.getElementById("width").value + document.getElementById("widthType").value + ";";
			// border
			style += "border:" + document.getElementById("border").value + "px";
			styleWithoutWidth += "border:" + document.getElementById("border").value + "px";
			// borderstyle
			if(formBorderStyle != "none") {
				style += " " + formBorderStyle;
				styleWithoutWidth += " " + formBorderStyle;
			}
			// border-color
			if(formBorderColor != "none") {
				style += " " + formBorderColor;
				styleWithoutWidth += " " + formBorderColor;
			}
			style += ";";
			styleWithoutWidth += ";";
			// border-collapse
			var collapse = document.getElementById("bordercollapse").checked ? "collapse" : "separate";
			style += "border-collapse:" + collapse + ";";
			styleWithoutWidth += "border-collapse:" + collapse + ";";
			// background-color
			if(formBackgroundColor != "none") {
				style += "background-color:" + formBackgroundColor + ";";
				styleWithoutWidth += "background-color:" + formBackgroundColor + ";";
			}
			
			WYSIWYG_Core.setAttribute(table, "style", style);

			if(tab != null) {
				var aTd = table.getElementsByTagName('TD');
				var nbCells = aTd.length;
				for (var i = 0; i < nbCells; i++) {
					WYSIWYG_Core.setAttribute(aTd[i], "style", styleWithoutWidth);
				};
				if ((table.children[0] != undefined || table.children[0] != null) && table.children[0].tagName == 'TBODY' ) {
					addOrDelRowsOrCols(doc,table.children[0],formNbCols,formNbRows,styleWithoutWidth);
				}
				if ((table.children[0] != undefined || table.children[0] != null) && table.children[0].tagName == 'TR' ) {
					addOrDelRowsOrCols(doc,table,formNbCols,formNbRows,styleWithoutWidth);
				}
			}
			for (var i = 0; i < table.rows.length; i++) {
				for (var j = 0; j < table.rows[i].childElementCount; j++) {
					table.rows[i].children[j].setAttribute('data-index',i+'-'+j);
				};
			};

			if(tab == null) {
				WYSIWYG_Table.create(n, table);
			}
			window.close();
			return;
			// Inserts the table code into the WYSIWYG editor	
			WYSIWYG.insertHTML(table, n);

		window.close();
	}	
	function createTD(doc,style) {
		var td = doc.createElement("td");
		WYSIWYG_Core.setAttribute(td, "style", style);
		td.innerHTML = "&nbsp;";
		return td;
	}
	function addOrDelRowsOrCols(doc,obj,formNbCols,formNbRows,style) {
		var nbRows = obj.childElementCount;
		// delete rows
		if (nbRows > formNbRows) {
			for (var i = (nbRows-1); i >= formNbRows; i--) {
				obj.removeChild(obj.children[i]);
			};
		}
		// add rows
		if (nbRows < formNbRows) {
			for (var i = nbRows; i < formNbRows; i++) {
				var tr = doc.createElement("tr");
				for(var j=0;j<formNbCols;j++){
					var td = createTD(doc,style);
					tr.appendChild(td);	
				}
				obj.appendChild(tr);
			};
		}
		var nbCols = obj.children[0].childElementCount;
		// delete cells
		if (nbCols > formNbCols) {
			for (var i = 0; i < formNbRows; i++) {
				for (var j = (nbCols-1); j >= formNbCols; j--) {
					obj.children[i].removeChild(obj.children[i].children[j]);
				};
			};
		}
		// add cells
		if (nbCols < formNbCols) {
			var colsToAdd = (formNbCols-nbCols);
			for (var i = 0; i < formNbRows; i++) {
				if (obj.children[i].childElementCount < formNbCols) {
					for(var j=0;j<colsToAdd;j++){
						var td = createTD(doc,style);
						obj.children[i].appendChild(td);	
					}
				}
			};
		}
	}
	/* ---------------------------------------------------------------------- *\
	  Function    : loadTable()
	  Description : load the settings of a selected Table into the form fields
	\* ---------------------------------------------------------------------- */
	function loadTable() {
		var n = WYSIWYG_Popup.getParam('wysiwyg');
		
		// get selection and range
		var sel = WYSIWYG.getSelection(n);
		var range = WYSIWYG.getRange(sel);

		// the current tag of range
		var tab = WYSIWYG.findParent("table", range);
		
		// if no table is defined then return
		if(tab == null) return;
	
		// if tbody tag is present
		if ((tab.children[0] != undefined || tab.children[0] != null) && tab.children[0].tagName == 'TBODY') {
			var nbRows = tab.children[0].childElementCount;
			document.getElementById('rows').value = nbRows;

			if (tab.children[0].children[0] != undefined || tab.children[0].children[0] != null) {
				var nbCols = tab.children[0].children[0].childElementCount;
				document.getElementById('cols').value = nbCols;
			}
		}
		//if tbody is absent
		if ((tab.children[0] != undefined || tab.children[0] != null) && tab.children[0].tagName == 'TR') {
			var nbRows = tab.childElementCount;
			document.getElementById('rows').value = nbRows;

			if (tab.children[0].children[0] != undefined || tab.children[0].children[0] != null) {
				var nbCols = tab.children[0].childElementCount;
				document.getElementById('cols').value = nbCols;
			}
		}

		// assign the values to the form elements
		for(var i = 0;i < tab.attributes.length;i++) {
			var attr = tab.attributes[i].name.toLowerCase();
			var value = tab.attributes[i].value;
			// alert(attr + " = " + value);
			if(attr && value && value != "null") {
				switch(attr) {
					case "style": 
						parseStyle(value);
					break;
					case "align":
						selectItemByValue(document.getElementById('alignment'), value);
					break;
					case "width":
						document.getElementById('width').value = value;
						selectItemByValue(document.getElementById('widthType'), 'px');
					break;		
				}
			}
		}
	}
	/* ---------------------------------------------------------------------- *\
	  Function    : selectItem()
	  Description : Select an item of an select box element by value.
	\* ---------------------------------------------------------------------- */
	function selectItemByValue(element, value) {
		if(element.options.length) {
			for(var i=0;i<element.options.length;i++) {
				if(element.options[i].value == value) {
					element.options[i].selected = true;
				}
			}
		}
	}
	/* ---------------------------------------------------------------------- *\
	  Function    : parseStyle()
	  Description : parse style and assign value to input field.
	\* ---------------------------------------------------------------------- */
	function parseStyle(value) {
		var styles = value.split(';');
		var nbStyles = styles.length;
		for (var i = 0; i < nbStyles; i++) {
			if (styles[i].indexOf('padding') !== -1) {
				var pad = styles[i].replace('padding','');
				pad = pad.replace(':','');
				document.getElementById('padding').value = pad.replace('px','').trim();
			}
			if (styles[i].indexOf('border') !== -1 && styles[i].indexOf('border-collapse') === -1) {
				var bord = styles[i].replace('border','');
				bord = bord.replace(':','');
				if (bord.indexOf('rgb') !== -1) {
					bord = bord.split('rgb');
					var colorRgb = bord[1];
					bord = bord[0];
					colorRgb = colorRgb.replace('(','').replace(')','');
					colorRgb = colorRgb.split(',');
					bord += convert2Hex(colorRgb[0].trim(),colorRgb[1].trim(),colorRgb[2].trim());
				}
				bord = bord.replace('px','').trim().split(' ');
				document.getElementById('border').value = bord[0];
				selectItemByValue(document.getElementById('borderstyle'),bord[1]);
				var elm = document.getElementById('bordercolor');
				if (bord[2] == undefined) {
					elm.style.color = '#FFF';
					elm.style.backgroundColor = '#FFF';
					elm.value = '';
				} else {
					elm.style.color = bord[2];
					elm.style.backgroundColor = bord[2];
					elm.value = bord[2];
				}
			}
			if (styles[i].indexOf('border-collapse') !== -1) {
				var bordcollapse = styles[i].replace('border-collapse','');
				bordcollapse = bordcollapse.replace(':','').trim();
				document.getElementById('bordercollapse').checked = (bordcollapse == 'collapse' ? true : false);
			}
			if (styles[i].indexOf('width') !== -1) {
				var W = styles[i].replace('width','');
				W = W.replace(':','');
				W = W.replace('%','');
				W = W.replace('px','').trim();
				document.getElementById('width').value = (W == '' ? '100' : W);
				if (styles[i].indexOf('px') !== -1) {
					selectItemByValue(document.getElementById('widthType'),'px');
				} else {
					selectItemByValue(document.getElementById('widthType'),'%');
				}
			}
			if (styles[i].indexOf('background-color') !== -1) {
				var background = styles[i].replace('background-color','');
				background = background.replace(':','').trim();
				var elm = document.getElementById('backgroundcolor');
				elm.style.color = background;
				elm.style.backgroundColor = background;
				elm.value = background;
			}
		};	
	}

	function fixHex(theDec) { 
	   var hNum = new Array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
	   var retHex = hNum[theDec];
	   return retHex;
	}
	function dec2hex(theDec) {
	   for (var k=0;k<theDec.length;k++) {
	      var thisChar = theDec.charAt(k);
	      if ( (thisChar < "0") || (thisChar > "9") ) {
	         alert("Code décimal (0-255) uniquement.");
	         return "BAD";
	      }
	   }
	   var leftNum;
	   var rightNum;
	   var leftNumS;
	   var rightNumS;
	   var retNum;
	   if (theDec > 255) {
	      alert("Pas plus que 255.");
	      return "BAD";
	   }
	   else {
	      leftNum = Math.floor(theDec / 16);
	      leftNumS = fixHex(leftNum);
	      rightNum = theDec%16;
	      rightNumS = fixHex(rightNum);
	      retNum = leftNumS + rightNumS;
	   }
	   return retNum;
	}
	function convert2Hex(numR,numG,numB,doneString) {      
	   if ( (!numR || !numG || !numB ) && (!doneString) ) {
	      alert("Entrez une valeur dans chaque case.");
	      return false;
	   }
	   hexRval = dec2hex(numR);
	   hexGval = dec2hex(numG);
	   hexBval = dec2hex(numB);
	   if ( (hexRval == "BAD") || (hexGval == "BAD") || (hexBval == "BAD") ) {
	      return false;
	   }
	   else {
	      hexStringOut =  "" + hexRval + hexGval + hexBval;
	      hexStringOut = hexStringOut.toUpperCase();
	      return "#" + hexStringOut;  
	   }
	}
	</script>

	</head>
	<body onload="WYSIWYG_ColorInst.init();loadTable();">

		<table border="0" cellpadding="0" cellspacing="0" style="width:100%;padding: 10px;">
			<tr>
				<td>
					<span style=" font-weight: bold;"><?php echo L_TABLE_PROPERTIES;?> :</span>

					<table style="width:100%;" border="0" cellpadding="1" cellspacing="0"
						class="table-settings">
						<tr>
							<td style="width: 20%;">
								<?php echo L_ROWS;?> :
							</td>
							<td style="width: 25%;">
								<input type="text" size="4" id="rows" name="rows" value="2" style="width: 50px;">
							</td>
							<td style="width: 25%;">
								<?php echo L_WIDTH;?> :
							</td>
							<td style="width: 30%;">
								<input type="text" name="width" id="width" value="100" size="10" style="width: 50px;">
								<select name="widthType" id="widthType"
									style="margin-right: 10px; font-size: 10px;">
									<option value="%">%</option>
									<option value="px">px</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<?php echo L_COLS;?> :
							</td>
							<td>
								<input type="text" size="4" id="cols" name="cols" value="2"	style="width: 50px;">
							</td>
							<td>
								<?php echo L_ALIGNMENT;?> :
							</td>
							<td>
								<select name="alignment" id="alignment" style="margin-right: 10px; width: 95px;">
									<option value=""></option>
									<option value="left"><?php echo L_LEFT;?></option>
									<option value="right"><?php echo L_RIGHT;?></option>
									<option value="center" selected="selected"><?php echo L_CENTER;?></option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<?php echo L_PADDING;?> :
							</td>
							<td>
								<input type="text" id="padding" name="padding" value="2" style="width: 50px;">
							</td>
							<td>
								<?php echo L_BACKGROUND_COLOR;?> :
							</td>
							<td>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="25">
											<table border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td>
														<input type="text" name="backgroundcolor" id="backgroundcolor" value="" style="width:50px;">
													</td>
												</tr>
											</table>
										</td>
										<td>
											<button style="margin-left: 2px;" onClick="WYSIWYG_ColorInst.choose('backgroundcolor');">
												<?php echo L_CHOOSE;?>
											</button>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
						<td>
							<?php echo L_BORDER_SIZE;?> :
						</td>
						<td>
							<input type="text" size="4" id="border" name="border" value="1" style="width: 50px;">
						</td>
						<td>
							<?php echo L_BORDER_COLOR;?> :
						</td>
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="25">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td>												
													<input type="text" name="bordercolor" id="bordercolor" value="" style="width:50px;">
												</td>
											</tr>
										</table>
									</td>
									<td>
										<button style="margin-left: 2px;" onClick="WYSIWYG_ColorInst.choose('bordercolor');">
											<?php echo L_CHOOSE;?>
										</button>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo L_BORDER_STYLE;?> :
						</td>
						<td>
							<select id="borderstyle" name="borderstyle" style="width: 80px;">
								<option value="none"></option>
								<option value="solid" selected="selected"><?php echo L_SOLID;?></option>
								<option value="double"><?php echo L_DOUBLE;?></option>
								<option value="dotted"><?php echo L_DOTTED;?></option>
								<option value="dashed"><?php echo L_DASHED;?></option>
								<option value="groove"><?php echo L_GROOVE;?></option>
								<option value="ridge"><?php echo L_RIDGE;?></option>
								<option value="inset"><?php echo L_INSET;?></option>
								<option value="outset"><?php echo L_OUTSET;?></option>
							</select>
						</td>
						<td>
							<?php echo L_BORDER_COLLAPSE;?> :
						</td>
						<td>
							<input type="checkbox" name="bordercollapse" id="bordercollapse" checked="checked">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
		<div align="right">
			<input type="submit" value="  <?php echo L_SUBMIT_BUTTON;?>  " onClick="buildTable();"
				style="font-size: 12px;">
			&nbsp;
			<input type="submit" value="  <?php echo L_CANCEL;?>  " onClick="window.close();"
				style="font-size: 12px; margin-right: 15px;">
		</div>
	</body>
</html>