<?php 
require_once '../../../prepend.php';

if(!defined('MS5E_ROOT')) exit;
?><!DOCTYPE html>

<html lang="<?php echo MS5E_LANG ?>">
<head>
<title>Preview</title>
<script src="<?php echo getUrl('js/openwysiwyg_v1.4.7/scripts/wysiwyg-popup.js');?>"></script>
<script>
function createCssLink(css) {
	var link = document.createElement("link");
		link.setAttribute("rel", "stylesheet");
		link.setAttribute("type", "text/css");
		link.setAttribute("href", css);
	return link;
}
function preview() {	
	// get params
	var n = WYSIWYG_Popup.getParam('wysiwyg');
	// set default styles
		
	if (WYSIWYG.config[n].DefaultPreviewStyle.lastIndexOf('.css') == -1) {
		WYSIWYG_Core.setAttribute(document.body, "style", WYSIWYG.config[n].DefaultStyle);
	} else {
		var linkDefault = createCssLink(WYSIWYG.config[n].DefaultPreviewStyle);
		var heads = document.getElementsByTagName("head");
		for(var i=0;i<heads.length;i++) {
			heads[i].appendChild(linkDefault);		
			if (WYSIWYG.config[n].CustomPreviewStyle.lastIndexOf('.css') != -1) {
				var linkCustom = createCssLink(WYSIWYG.config[n].CustomPreviewStyle);
				heads[i].appendChild(linkCustom);		
			}
			break;
		}
	}
	var prepend = '<div class="layout">\
    <div id="controls"><!-- DO NOT EDIT --></div>\
    <div id="currentSlide"><!-- DO NOT EDIT --></div>\
    <div id="header"></div>\
    <?php echo preg_replace("#(\n|\t|\r)#","\\\\$1",file_get_contents(MS5E_SLIDES_PATH.'footer.php'));?>\
</div><div class="xoxo presentation"><div class="slide" style="visibility:visible;">';
	var append = '</div></div>';
	// get content
	WYSIWYG_Table.disableHighlighting(n);
	var content = WYSIWYG.getEditorWindow(n).document.body.innerHTML;
	WYSIWYG_Table.refreshHighlighting(n);
	content = content.replace(/src="([^/|^http|^https])/gi, "src=\"../$1"); // correct relative image path
	content = content.replace(/href="([^/|^http|^https|^ftp])/gi, "href=\"../$1"); // correct relative anchor path
	// set content
	document.body.innerHTML = prepend + content + append;
}
</script>
</head>
<body onLoad="preview();">
	
</body>
</html> 

