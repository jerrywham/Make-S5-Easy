/********************************************************************
 * openWYSIWYG settings file Copyright (c) 2006 openWebWare.com
 * Contact us at devs@openwebware.com
 * This copyright notice MUST stay intact for use.
 *
 * $Id: wysiwyg-settings.js,v 1.4 2007/01/22 23:05:57 xhaggi Exp $
 ********************************************************************/
function widthAndHeight(whatDim,elemId) {
    if (elemId != undefined) {
        if (whatDim == 'width') {
            return  document.getElementById('main').clientWidth;
        } else if (whatDim == 'height') {
            return  document.getElementById('main').clientHeight;
        } else {
            return [document.getElementById('main').clientWidth, document.getElementById('main').clientHeight];
        }
    }
    var winW = 600, winH = 600;
    if (document.body && document.body.offsetWidth) {
        winW = document.body.offsetWidth;
        winH = document.body.offsetHeight;
    }
    if (document.compatMode=='CSS1Compat' &&
        document.documentElement &&
        document.documentElement.offsetWidth ) {
        winW = document.documentElement.offsetWidth;
        winH = document.documentElement.offsetHeight;
    }
    if (window.innerWidth && window.innerHeight) {
        winW = window.innerWidth;
        winH = window.innerHeight;
    }
    if (whatDim == 'width') {
        return winW;
    } else if (whatDim == 'height') {
        return winH;
    } else {
        return [winW,winH];
    }
}


/*
 * Full featured setup used the openImageLibrary addon
 */
// var paramsOfUrl = window.location.href;
var url = window.location.href;
var reg = new RegExp('index\.php.*');
url = url.replace(reg,'',url);
    
var editorDir = 'js/openwysiwyg_v1.4.7/';
var imgExtLink = '&nbsp;&nbsp;<img src="'+url+editorDir+'img/lien.png" alt="lien externe" title="Ouverture du lien dans un nouvel onglet ou une nouvelle fenêtre" class="noborder" />';
var Trad = [];
Trad['bold'] = 'Gras';
Trad['italic'] = 'Italique';
Trad['underline'] = 'Souligner';
Trad['strikethrough'] = 'Barrer';
Trad['seperator'] = 'Séparateur';
Trad['subscript'] = 'Indice';
Trad['superscript'] = 'Exposant';
Trad['justifyleft'] = 'Aligner à gauche';
Trad['justifycenter'] = 'Centrer';
Trad['justifyright'] = 'Aligner à droite';
Trad['justifyfull'] = 'Justifier';
Trad['unorderedlist'] = 'Liste non ordonnée';
Trad['orderedlist'] = 'Liste ordonnée';
Trad['incrementalInsertUnorderedList'] = 'Liste non ordonnée incrémentale';
Trad['incrementalInsertOrderedList'] = 'Liste ordonnée incrémentale';
Trad['incrementalInsertUnorderedListWithHighlightFirst'] = 'Liste non ordonnée incrémentale avec le premier item mis en évidence';
Trad['incrementalInsertOrderedListWithHighlightFirst'] = 'Liste ordonnée incrémentale avec le premier item mis en évidence';
Trad['outdent'] = 'Diminuer le retrait';
Trad['indent'] = 'Augmenter le retrait';
Trad['cut'] = 'Couper';
Trad['copy'] = 'Copier';
Trad['paste'] = 'Coller';
Trad['forecolor'] = 'Couleur du texte';
Trad['backcolor'] = 'Couleur du fond';
Trad['undo'] = 'Annuler';
Trad['redo'] = 'Rétablir';
Trad['insertparagraph'] = 'Transformer la sélection en paragraphe';
Trad['inserttable'] = 'Insérer un tableau';
Trad['insertimage'] = 'Insérer une image';
Trad['insertdocument'] = 'Insérer un document';
Trad['createlink'] = 'Ajouter un lien';
Trad['viewSource'] = 'Voir le code source';
Trad['viewText'] = 'Voir le texte';
Trad['help'] = 'Aide';
Trad['fonts'] = 'Polices';
Trad['fontsizes'] = 'Taille de la police';
Trad['headings'] = 'Titres';
Trad['preview'] = 'Prévisualisation';
Trad['print'] = 'Imprimer';
Trad['removeformat'] = 'Supprimer le formatage issu de Microsoft Word';
Trad['delete'] = 'Supprimer';
Trad['save'] = 'Enregistrer uniquement le contenu du formulaire ci-dessous';
Trad['saveall'] = 'Enregistrer';
Trad['return'] = 'Retour';
Trad['maximize'] = 'Maximizer';
Trad['modifyTable'] = 'Modifier les propriétés du tableau&hellip;';
Trad['modifyImg'] = 'Modifier les propriétés de l\'image&hellip;';
Trad['modifyDoc'] = 'Modifier ou ajouter un document&hellip;';
Trad['modifyLink'] = 'Créer ou modifier un lien&hellip;';
Trad['removenode'] = 'Supprimer la mise en forme';
Trad['mergecells'] = 'Fusionner les cellules';
Trad['splitcellsH'] = 'Scinder les colonnes';
Trad['splitcellsV'] = 'Scinder les lignes';
Trad['RemoveFormatConfMessage'] = 'Nettoyer le HTML ajouté par MS Word';
Trad['NoValidBrowserMessage'] = 'openWYSIWYG n\'est pas compatible avec votre navigateur';
Trad['CommandDisabled'] = 'Vous êtes en mode TEXTE. Cette commande est désactivée.';
Trad['warningRightContentWillBeDeleted'] = 'Attention, le contenu des cellules de droite sera supprimé. Continuer ?';
Trad['warningContentWillBeDeleted'] = 'Attention, seul le contenu de la cellule du haut à gauche sera conservé.';
Trad['warningMergeNotAllowed'] = 'Attention, ce type de fusion n\'est pas possible'; 

var DefaultStyle = "\
	color: #444;\
	font-family: helvetica, arial, sans-serif;\
	font-size: 16px;\
	line-height: 1.4;\
";
    
var widthOfTextarea = '99%';
// For heightOfTextarea
//80 is height of toolbars
//42 is height of h2 tag
//50 is margin-bottom
var heightOfTextarea = ((widthAndHeight('height','main')-(2*80)-(2*42)-(50*2))/2);
if (heightOfTextarea < 250) heightOfTextarea = 250;
heightOfTextarea = ''+heightOfTextarea+'px';


var full = new WYSIWYG.Settings();
full.TradToolbar = Trad;
full.ImagesDir = url+editorDir+"images/";
full.PopupsDir = url+editorDir+"popups/";
full.DefaultStyle = DefaultStyle;
full.DefaultPreviewStyle = url+'data/presentation/ui/default/slides.css';
full.CustomPreviewStyle = '';
full.PreviewWidth = 1160;
full.PreviewHeight = 750;
full.CSSFile = url+editorDir+"styles/wysiwyg.css";
full.Width = widthOfTextarea; 
full.Height = heightOfTextarea;
full.addToolbarElement("seperator", 1, 19); 
full.addToolbarElement("incrementalInsertUnorderedList", 1, 20); 
full.addToolbarElement("incrementalInsertOrderedList", 1, 21); 
full.addToolbarElement("incrementalInsertUnorderedListWithHighlightFirst", 1, 22); 
full.addToolbarElement("incrementalInsertOrderedListWithHighlightFirst", 1, 23); 
full.addToolbarElement("seperator", 1, 24); 
full.addToolbarElement("createlink", 1, 25); 
full.addToolbarElement("font", 3, 1); 
full.addToolbarElement("fontsize", 3, 2);
full.addToolbarElement("headings", 3, 3);
full.ImagePopupFile = url+editorDir+"addons/imagelibrary/insert_image.php";
full.DocumentPopupFile = url+editorDir+"addons/documentlibrary/insert_document.php";
full.ImagePopupWidth = 800;
full.ImagePopupHeight = 480;
full.DocumentPopupWidth = 800;
full.DocumentPopupHeight = 480;
full.Toolbar[1][7] = '';
full.Toolbar[1][8] = '';
full.Toolbar[1][15] = '';

full.RemoveFormatConfMessage = Trad['RemoveFormatConfMessage'];
full.NoValidBrowserMessage = Trad['NoValidBrowserMessage'];
full.CommandDisabled = Trad['CommandDisabled'];
full.warningRightContentWillBeDeleted = Trad['warningRightContentWillBeDeleted'];
full.warningContentWillBeDeleted = Trad['warningContentWillBeDeleted'];
full.warningMergeNotAllowed = Trad['warningMergeNotAllowed'];
 		
/*
 * Small Setup Example
 */
var small = new WYSIWYG.Settings();
small.ImagesDir = url+editorDir+"images/";
small.PopupsDir = url+editorDir+"popups/";
small.DefaultStyle = DefaultStyle;
small.DefaultPreviewStyle = url+'data/presentation/ui/default/slides.css';
small.CustomPreviewStyle = '';
small.PreviewWidth = 1160;
small.PreviewHeight = 750;
small.CSSFile = url+editorDir+"styles/wysiwyg.css";
small.Width = widthOfTextarea; 
small.Height = heightOfTextarea;
// customize toolbar buttons
small.addToolbarElement("font", 3, 1); 
small.addToolbarElement("fontsize", 3, 2);
small.addToolbarElement("headings", 3, 3);
small.addToolbarElement("viewSource", 3, 4);
small.addToolbarElement("maximize", 3, 5);
small.TradToolbar = Trad;
small.RemoveFormatConfMessage = Trad['RemoveFormatConfMessage'];
small.NoValidBrowserMessage = Trad['NoValidBrowserMessage'];
small.CommandDisabled = Trad['CommandDisabled'];
small.warningRightContentWillBeDeleted = Trad['warningRightContentWillBeDeleted'];
small.warningContentWillBeDeleted = Trad['warningContentWillBeDeleted'];
small.warningMergeNotAllowed = Trad['warningMergeNotAllowed'];
small.Toolbar[0] = new Array("bold","italic","underline","strikethrough","inserttable","removeformat","seperator","undo","redo","seperator","justifyleft","justifycenter","justifyright","justifyfull","seperator","unorderedlist","orderedlist","outdent","indent","seperator","createlink" ); // small setup for toolbar 1
small.Toolbar[1] = ""; // disable toolbar 2
small.StatusBarEnabled = true;