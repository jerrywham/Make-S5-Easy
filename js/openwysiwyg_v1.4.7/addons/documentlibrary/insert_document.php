<?php
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SIGesTH
#
# Copyright (c) 2009 - 2014 Cyril MAGUIRE, <contact@ecyseo.net>
# Licensed under the CeCILL v2.1 license.
# See http://www.cecill.info/licences.fr.html
#
# ------------------- END LICENSE BLOCK -------------------

require 'config.inc.php';

if(!defined('MS5E_ROOT')) exit;

// error_reporting(0);
// get the identifier of the editor
if (isset($_GET['wysiwyg'])) {
	$wysiwyg = $_GET['wysiwyg']; 
} else {
	throw new Exception('Error variable $wysiwyg undefined', 1);
	
}

if($leadon=='.') $leadon = '';
if((substr($leadon, -1, 1)!='/') && $leadon!='') $leadon = $leadon . '/';
$startdir = $leadon;

// validate the directory
if (isset($_GET['dir'])) {
	$_GET['dir'] = $_POST['dir'] ? Tools::nullbyteRemove($_POST['dir']) : Tools::nullbyteRemove($_GET['dir']);
} else {
	$_GET['dir'] = null;
}
if($_GET['dir']) {
	if(substr($_GET['dir'], -1, 1)!='/') {
		$_GET['dir'] = $_GET['dir'] . '/';
	}
	$dirok = true;
	$dirnames = explode('/', $_GET['dir']);
	for($di=0; $di<sizeof($dirnames); $di++) {
		if($di<(sizeof($dirnames)-2)) {
			$dotdotdir = $dotdotdir . $dirnames[$di] . '/';
		}
	}
	if(substr($_GET['dir'], 0, 1)=='/') {
		$dirok = false;
	}

	if($_GET['dir'] == $leadon) {
		$dirok = false;
	}
	
	if($dirok) {
		$leadon = $_GET['dir'];
	}
}
?><!DOCTYPE html>
<html lang="<?php echo MS5E_LANG ?>">
<head>
<title>openWYSIWYG | <?php echo L_INSERT_DOCUMENT?></title>

<script type="text/javascript" src="<?php echo getUrl('js/openwysiwyg_v1.4.7/scripts/wysiwyg-popup.js');?>"></script>
<script language="JavaScript" type="text/javascript">

/* ---------------------------------------------------------------------- *\
  Function    : insertDocument()
  Description : Inserts document into the WYSIWYG.
\* ---------------------------------------------------------------------- */
function insertDocument() {
	var n = WYSIWYG_Popup.getParam('wysiwyg');
	
	// get values from form fields
	var url = '<?php echo getUrl('data/medias/');?>';
	var href = document.getElementById('href').value.replace(url,'');
	var align = document.getElementById('align').value;
	var alt = document.getElementById('alt').value;
	
	if (alt == '') {
		var docName = href.lastIndexOf('-MS5E-');
		var ext = href.lastIndexOf('.');
		if(docName != -1) {
			alt = href.substring(docName,0,href.length);
		} else {
			alt = href;
		}
		if (ext != -1 && alt != href) {
			ext = href.substring(ext,href.length);
			alt += ext;
		}
		var path = alt.lastIndexOf('/');
		if (path != -1) {
			alt = alt.substring(path + 1,alt.length);
		}
		var p = alt.lastIndexOf('.pages.zip');
		if (p != -1) {
			alt = alt.substring(p,0,alt.length)+'.pages';
		}
		p = alt.lastIndexOf('.numbers.zip');
		if (p != -1) {
			alt = alt.substring(p,0,alt.length)+'.numbers';
		}
	}
	href = url + href;

	// insert image
	WYSIWYG.insertDocument(href, alt, align, n);
  	window.close();
}

/* ---------------------------------------------------------------------- *\
  Function    : loadDocument()
  Description : load the settings of a selected document into the form fields
\* ---------------------------------------------------------------------- */
function loadDocument() {
	var n = WYSIWYG_Popup.getParam('wysiwyg');
	
	// get selection and range
	var sel = WYSIWYG.getSelection(n);
	var range = WYSIWYG.getRange(sel);
	
	// the current tag of range
	var doc = WYSIWYG.findParent("a", range);
				
	// if no document is defined then return
	if(doc == null) return;

	selectItemByValue(document.getElementById('align'), doc.align);
		
	// assign the values to the form elements
	for(var i = 0;i < doc.attributes.length;i++) {
		var attr = doc.attributes[i].name.toLowerCase();
		var value = doc.attributes[i].value;
		// alert(attr + " = " + value);
		if(attr && value && value != "null") {
			switch(attr) {
				case "href": 
					// strip off urls on IE
					if(WYSIWYG_Core.isMSIE) value = WYSIWYG.stripURLPath(n, value, false);
					var posOfFiles = value.indexOf('<?php echo $docbaseurl;?>/');
					document.getElementById('href').value = value.substr(posOfFiles);
				break;
				case "align":
					selectItemByValue(document.getElementById('align'), value);
				break;			
			}
		}
	}
	document.getElementById('alt').value = doc.innerHTML.replace('<br>','');
}

/* ---------------------------------------------------------------------- *\
  Function    : selectItem()
  Description : Select an item of an select box element by value.
\* ---------------------------------------------------------------------- */
function selectItemByValue(element, value) {
	if(element.options.length) {
		for(var i=0;i<element.options.length;i++) {
			if(element.options[i].value == value) {
				element.options[i].selected = true;
			}
		}
	}
}

</script>
</head>
<body onLoad="loadDocument();">
<form method="post" action="<?php echo getUrl('addons/documentlibrary/insert_document.php');?>?wysiwyg=<?php echo $wysiwyg; ?>" enctype="multipart/form-data">
<table cellpadding="0" cellspacing="0" style="padding: 10px;border:1px solid #ccc;">
<tr>
	<td>
		<input type="hidden" id="dir" name="dir" value="">
		<span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;"><?php echo L_INSERT_DOCUMENT?> :</span>
		<table cellpadding="0" cellspacing="0" style="width:100%;background-color: #F7F7F7; border: 2px solid #FFFFFF; padding: 5px;margin-bottom:50px">
			<tr>
				<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;" width="80"><?php echo L_DOCUMENT_URL;?> :</td>
				<td style="padding-bottom: 2px; padding-top: 0px;" width="300"><input type="text" name="href" id="href" value=""  style="font-size: 10px; width: 100%;"></td>
				<td style="padding-top: 0px;padding-bottom: 2px;font-family: tahoma; font-size: 9px;">&nbsp;</td>
			</tr>
			<tr>
				<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;" width="80"><?php echo L_ALTERNATE_TEXT;?> :</td>
				<td style="padding-bottom: 2px; padding-top: 0px;" width="300"><input type="text" name="alt" id="alt" value=""  style="font-size: 10px; width: 100%;"></td>
				<td style="padding-top: 0px;padding-bottom: 2px;font-family: tahoma; font-size: 9px;">&nbsp;</td>
			</tr>
			<tr>
				<td style="width: 115px;padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;" width="100"><?php echo L_ALIGNMENT;?> :</td>
				<td style="width: 85px;padding-bottom: 2px; padding-top: 0px;">
					<select name="align" id="align" style="font-family: arial, verdana, helvetica; font-size: 11px; width: 100%;">
						<option value="">&nbsp;</option>
						<option value="left"><?php echo L_LEFT;?></option>
						<option value="right"><?php echo L_RIGHT;?></option>
						<option value="center"><?php echo L_CENTER;?></option>
					</select>
				</td>
			</tr>
		</table>
	</td>
	<td style="vertical-align: top;width: 50%; padding-left: 5px;">
		<iframe id="chooser" frameborder="0" style="height:380px;width: 100%;border: 2px solid #FFFFFF; padding: 5px;" src="<?php echo getUrl('js/openwysiwyg_v1.4.7/addons/documentlibrary/select_document.php');?>?wysiwyg=<?php echo $wysiwyg;?>&dir=<?php echo $leadon; ?>"></iframe>
	</td>
</tr>
<tr>
	<td colspan="2" style="padding-top: 5px;padding-right:20px;text-align: right;">
		<input type="submit" value="  <?php echo L_SUBMIT_BUTTON;?>  " onclick="insertDocument();return false;" style="font-size: 12px;">
		<input type="button" value="  <?php echo L_CANCEL;?>  " onclick="window.close();" style="font-size: 12px;">	
	</td>
</tr>
</table>
</form>
</body>
</html>