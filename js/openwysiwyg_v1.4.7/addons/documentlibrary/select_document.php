<?php
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SIGesTH
#
# Copyright (c) 2009 - 2014 Cyril MAGUIRE, <contact@ecyseo.net>
# Licensed under the CeCILL v2.1 license.
# See http://www.cecill.info/licences.fr.html
#
# ------------------- END LICENSE BLOCK -------------------


require('config.inc.php');

if(!defined('MS5E_ROOT')) exit;

if (!isset($msg)) {
	$msg = '';
}
if (isset($_GET['wysiwyg'])) {
	$wysiwyg = $_GET['wysiwyg'];
} else {
	throw new Exception("Error Processing Request", 1);
	
}

$dotdotdir = '';
$dirok = false;

// error_reporting(0);
if((substr($docbaseurl, -1, 1)!='/') && $docbaseurl!='') $docbaseurl = $docbaseurl . '/';
if((substr($docbasedir, -1, 1)!='/') && $docbasedir!='') $docbasedir = $docbasedir . '/';
$leadon = '';
if($leadon=='.') $leadon = '';
if((substr($leadon, -1, 1)!='/') && $leadon!='') $leadon = $leadon . '/';
$startdir = MS5E_FILES_PATH;

if (isset($_GET['d']) && isset($_GET['dir']) ) {
	$dir = nullbyteRemove(str_replace('.','',$_GET['dir']));
	$docToDel = nullbyteRemove(str_replace('/','',$_GET['d']));

	if (is_file(MS5E_FILES_PATH.$dir.$docToDel) ) {
		if (false != $ext = array_search($finfo->file(MS5E_FILES_PATH.$dir.$docToDel),$supportedextentions,true)) {
			unlink(MS5E_FILES_PATH.$dir.$docToDel);
		} else {
			unset($_GET['d']);
		}
	} else {
		unset($_GET['d']);
	}
} else {
	unset($_GET['d']);
}

if(isset($_GET['dir'])) {
	if(substr($_GET['dir'], -1, 1)!='/') {
		$_GET['dir'] = nullbyteRemove($_GET['dir']) . '/';
	}
	$dirok = true;
	$dirnames = explode('/', $_GET['dir']);
	for($di=0; $di<sizeof($dirnames); $di++) {
		if($di<(sizeof($dirnames)-2)) {
			$dotdotdir = $dotdotdir . $dirnames[$di] . '/';
		}
	}
	if(substr($_GET['dir'], 0, 1)=='/') {
		$dirok = true;
	}

	if($_GET['dir'] == $leadon) {
		$dirok = false;
	}
	
	if($dirok) {
		$leadon = $_GET['dir'];
	}
	$actualDir = substr($leadon,0,strrpos(substr($leadon,0,-1),'/'));
}

//////////////
$phpallowuploads = $up['phpallowuploads'];
if (isset($_FILES['file']) ) {
	$leadon = $up['leadon'];
	$msg = $up['msg'];
	$dirok = true;
	$dirnames = explode('/', $leadon);
	for($di=0; $di<sizeof($dirnames); $di++) {
		if($di<(sizeof($dirnames)-2)) {
			$dotdotdir = $dotdotdir . $dirnames[$di] . '/';
		}
	}
}
$actualDir = substr($leadon,0,strrpos(substr($leadon,0,-1),'/'));
$opendir = MS5E_FILES_PATH.$leadon;
if(!$leadon) $opendir = MS5E_FILES_PATH;
if(!file_exists($opendir)) {
	$opendir = MS5E_FILES_PATH;
	$leadon = $startdir;
}
if ($leadon == '') {
	$leadon = $startdir;
}

clearstatcache();
if ($handle = opendir($opendir)) {
	$n = 0;
	while (false !== ($file = readdir($handle))) { 
		//first see if this file is required in the listing
		if ($file == "." || $file == ".." || $file == ".DS_Store" || $file == '.thumbs')  continue;
		if (@filetype($opendir.$file) == "dir") {
			if(!$browsedirs) continue;
		
			$n++;
			if(isset($_GET['sort']) && $_GET['sort']=="date") {
				$key = @filemtime($opendir.$file) . ".$n";
			}
			else {
				$key = $n;
			}
			$dirs[$key] = $file . "/";
		}
		else {
			$n++;
			if(isset($_GET['sort']) && $_GET['sort']=="date") {
				$key = @filemtime($opendir.$file) . ".$n";
			}
			elseif(isset($_GET['sort']) && $_GET['sort']=="size") {
				$key = @filesize($opendir.$file) . ".$n";
			}
			else {
				$key = $n;
			}
			$files[$key] = $file;
		}
	}
	closedir($handle); 
}
if ($leadon == $startdir) {
	$leadon = '';
}
//////////////

//sort our files
if (isset($_GET['sort'])) {
	if($_GET['sort']=="date") {
		@ksort($dirs, SORT_NUMERIC);
		@ksort($files, SORT_NUMERIC);
	}
	elseif($_GET['sort']=="size") {
		@natcasesort($dirs); 
		@ksort($files, SORT_NUMERIC);
	}
	else {
		@natcasesort($dirs); 
		@natcasesort($files);
	}
}
//order correctly
if(isset($_GET['order']) && $_GET['order']=="desc" && $_GET['sort']!="size") {$dirs = @array_reverse($dirs);}
if(isset($_GET['order']) && $_GET['order']=="desc") {$files = @array_reverse($files);}
$dirs = @array_values($dirs); $files = @array_values($files);
?><!DOCTYPE html>

<html lang="<?php echo MS5E_LANG ?>">
<head>
<title>openWYSIWYG | <?php echo L_SELECT_DOCUMENT;?></title>
<style type="text/css">
body {
	margin: 0px;
    overflow-x: hidden;
}
a {
	font-family: Arial, verdana, helvetica; 
	font-size: 11px; 
	color: #000;
	text-decoration: none;
	display: inline-block;
	width:300px;
}
a:hover {
	text-decoration: underline;
}
.del {
	width: auto;
}
#breadcrumbs {
	display: inline-block;
	font-size:10px;
	font-family:Tahoma;
	position: fixed;
	width:100%;
	background-color: #FFF;
	top:119px;
}
#breadcrumbs a {
	width: auto
}
#send {
	position:fixed;
	background-color: #FFF;
	height:90px;
	top:0;
}
h2 {
	padding:0;
	margin: 0;
	top:90px;
	position:fixed;
	background-color: #FFF;
	width:100%;
}
h2 {
	padding:0;
	margin: 0;
	top:90px;
	position:fixed;
	background-color: #FFF;
	width:100%;
}
p {
	margin-top:140px;
	overflow-x:hidden; 
}
.error {
	display: inline-block;
	background-color:#C95A52;
	color:#FFF;
	padding:5px;
	text-align: center;
	margin-top: 2px;
	width:100%;
}
</style>
<script type="text/javascript">
	var selectDocument = function(url) {
		if(parent) {
			parent.document.getElementById("href").value = url;
		}
	}
	
	if(parent) {
		parent.document.getElementById("dir").value = '<?php echo $leadon; ?>';
	}
	
	var waiting = function(evt) {
		var anime = document.getElementById('animation');
		anime.style.visibility = 'visible';
	};
	
</script>
</head>
<body>
	<?php
		if($allowuploads) {?>
			<form method="post" action="<?php echo $racine.'select_document.php';?>?wysiwyg=<?php echo $wysiwyg; ?>" enctype="multipart/form-data" id="send">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td>
						<input type="hidden" name="dir" value="">
						<span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;"><?php echo L_UPLOAD_DOC_ON_SERVER;?> :</span>
						<table cellpadding="0" cellspacing="0" style="width:100%;background-color: #F7F7F7; border: 2px solid #FFFFFF; padding: 5px;">
					<?php 
					if($phpallowuploads) {?>

							<tr>
								<td style="padding-top: 0px;padding-bottom: 0px;width:300px;">
									<input type="file" name="file" size="30" style="font-size: 10px; width: 87%;"/>
								</td>
								<td style="width:23%;"><input type="submit" value="<?php echo L_UPLOAD; ?>" id="submitButton"  onclick="waiting();" style="display:inline-block;">&nbsp;<img src="<?php echo getUrl('js/openwysiwyg_v1.4.7/');?>images/loading.gif" alt="work in progress..." id="animation" style="visibility: hidden;display:inline-block;margin-bottom:-4px;"/></td>
							</tr>
							<tr>
								<td style="padding-top: 0px;padding-bottom: 20px;font-family: tahoma; font-size: 11px;" colspan="2"><?php echo $msg; ?>
								<input type="hidden" name="lead" value="<?php echo $leadon ?> "></td>
							</tr>
				<?php
					}
					else {
				?>

							<tr>
								<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;" colspan="2">
									File uploads are disabled in your php.ini file. Please enable them.
								</td>
							</tr>
				<?php
					}?>

						</table>
					</td>
				</tr>
			</table>
			</form>
		<?php 
		}?>

	<h2><span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;"><?php echo L_SELECT_DOCUMENT;?> :</span></h2>
		<p>
	<?php
 	$breadcrumbs = explode('/', $leadon);
 	$breadcrumbsDir = explode('/',$actualDir);
  	if(($bsize = sizeof($breadcrumbs)) > 0) {
  		if(($bsize-1) > 0) {	
	  		echo '<div id="breadcrumbs"><a href="'.$racine.'select_document.php?wysiwyg='.$wysiwyg.'&dir=">documents</a>&nbsp;';
	  		$sofar = '';
	  		for($bi=0;$bi<($bsize-1);$bi++) {
				$sofar = $sofar . $breadcrumbs[$bi] . '/';
				echo (isset($breadcrumbsDir[$bi]) && $breadcrumbsDir[$bi] == $breadcrumbs[$bi]) ? '<a href="'.$racine.'select_document.php?wysiwyg='.$wysiwyg.'&dir='.urlencode($sofar).'">&raquo; '.$breadcrumbs[$bi].'</a>&nbsp;' : ' <strong>'.$breadcrumbs[$bi].'</strong>';
			}
			echo "</div>";
  		}
  	}

	$class = 'b';
	if($dirok) {
	?>

	<a href="<?php echo $racine.'select_document.php?wysiwyg='.$wysiwyg.'&dir='.urlencode($dotdotdir); ?>"><img src="<?php echo $racine.'images/folder.png';?>" alt="Folder" border="0" /> <strong>..</strong></a><br>
	<?php
		if($class=='b') $class='w';
		else $class = 'b';
	}
	$arsize = sizeof($dirs);
	for($i=0;$i<$arsize;$i++) {
		$dir = substr($dirs[$i], 0, strlen($dirs[$i]) - 1);
	?>

	<a href="<?php echo $racine.'select_document.php?wysiwyg='.$wysiwyg.'&dir='.urlencode($leadon.$dirs[$i]); ?>"><img src="<?php echo $racine;?>images/folder.png" alt="<?php echo $dir; ?>" border="0" /> <strong><?php echo $dir; ?></strong></a><br>
	<?php
		if($class=='b') $class='w';
		else $class = 'b';	
	}

	$arsize = sizeof($files);
	for($i=0;$i<$arsize;$i++) {
		$icon = 'unknown.png';
	    if (false != $ext = array_search($finfo->file($opendir.$files[$i]),$supportedextentions,true)) {			
			$thumb = '';
			if($filetypes[$ext]) {
				$icon = $filetypes[$ext];
			}

			$filename = substr($files[$i],0,strpos($files[$i], '-SIG-'));
			if ($filename == '') {
				$filename = $files[$i];
			}
			$lext = -strlen($ext);
			if (substr($filename, $lext) == $ext) {
				$filename = substr($filename,0,$lext-1);
			}
			if (strpos($filename, '.pages')) {
				$filename = str_replace('.pages','',$filename);
				$ext = 'pages';
				$icon = $filetypes['pages'];
			}
			if (strpos($filename, '.numbers')) {
				$filename = str_replace('.numbers','',$filename);
				$ext = 'numbers';
				$icon = $filetypes['numbers'];
			}
			$name = $filename;
			if (strlen($filename)>35) {
				$filename = substr($files[$i], 0, 35) . '...';
			}
			$fileurl = $leadon . $files[$i];
			$filedir = str_replace($docbasedir, "", $leadon);
			$filepath = $docbaseurl.$filedir.$files[$i];
	?>

	<a href="javascript:void(0)" onclick="selectDocument('<?php echo $filepath; ?>');" title="<?php echo $name;?>"><img src="<?php echo $racine.'images/'.$icon; ?>" alt="<?php echo $files[$i]; ?>" border="0" /> <strong><?php echo $filename.'.'.$ext; ?></strong></a> &nbsp;<a href="<?php echo $racine.'select_document.php?wysiwyg='.$wysiwyg.'&dir='.urlencode($sofar).'&d='.urlencode($files[$i]); ?>" onclick="return confirm('<?php echo L_CONFIRM_DELETE ?>');" class="del">&cross;</a><br/>
	<?php
			if($class=='b') $class='w';
			else $class = 'b';	
		}
	}	
	?>

</body>
</html>