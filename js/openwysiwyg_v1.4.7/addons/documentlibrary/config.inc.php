<?php
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SIGesTH
#
# Copyright (c) 2009 - 2014 Cyril MAGUIRE, <contact@ecyseo.net>
# Licensed under the CeCILL v2.1 license.
# See http://www.cecill.info/licences.fr.html
#
# ------------------- END LICENSE BLOCK -------------------
require_once '../prepend.php';

if(!defined('MS5E_ROOT')) exit;
/*
 * Path to a directory which holds the documents.
 */
// $docbasedir = '../../uploads';
$docbasedir = MS5E_FILES_PATH;

/*
 * An absolute or relative URL to the image folder.
 * This url is used to generate the source of the image.
 */
$docbaseurl = 'files';

/*
 * Allow your users to browse the subdir of the defined basedir.
 */
$browsedirs = true;

/*
 * If enabled users will be able to upload 
 * files to any viewable directory. You should really only enable
 * this if the area this script is in is already password protected.
 */

$allowuploads = true;


/*
 * If a user uploads a file with the same
 * name as an existing file do you want the existing file
 * to be overwritten?
*/
$overwrite = false;

/*
 * Define the extentions you want to show within the 
 * directory listing. The extensions also limit the 
 * files the user can upload to your document folders.   
 */
$supportedextentions = array(
	'csv' => 'text/csv',
	'doc' => 'application/msword',
	'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	'dot' => 'application/msword',
	'key' => 'application/octet-stream',
	'numbers' => 'application/octet-stream',
	'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
	'odt' => 'application/vnd.oasis.opendocument.text',
	'odp' => 'application/vnd.oasis.opendocument.presentation',
	'otp' => 'application/vnd.oasis.opendocument.presentation-template',
	'ott' => 'application/vnd.oasis.opendocument.text-template',
	'pages' => 'application/octet-stream',
	'pdf' => 'application/pdf',
	'pot' => 'application/vnd.ms-powerpoint',
	'pps' => 'application/vnd.ms-powerpoint',
	'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
	'ppt' => 'application/vnd.ms-powerpoint',
	'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
	'sig' => 'application/octet-stream',
	'sql' => 'application/octet-stream',
	'txt' => 'text/plain',
	'xl' => 'application/vnd.ms-excel',
	'xla' => 'application/vnd.ms-excel',
	'xlb' => 'application/vnd.ms-excel',
	'xlb' => 'application/x-excel',
	'xlc' => 'application/vnd.ms-excel',
	'xld' => 'application/vnd.ms-excel',
	'xlk' => 'application/vnd.ms-excel',
	'xll' => 'application/vnd.ms-excel',
	'xlm' => 'application/vnd.ms-excel',
	'xls' => 'application/vnd.ms-excel',
	'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	'xlt' => 'application/vnd.ms-excel',
	'xlv' => 'application/vnd.ms-excel',
	'xlw' => 'application/x-excel',
	'zip' => 'application/zip',
);
			
/*
 * If you want to add your own special file icons use 
 * this section below. Each entry relates to the extension of the 
 * given file, in the form <extension> => <filename>. 
 * These files must be located within the dlf directory.
 */
$filetypes = array (
	'csv' => 'open_office_calc.png',
	'doc' => 'page_word.png',
	'dot' => 'page_word.png',
	'docx' => 'page_word.png',
	'key' => 'presentation.png',
	'numbers' => 'chart_bar.png',
	'ods' => 'open_office_calc.png',
	'odt' => 'open_office_writer.png',
	'odp' => 'page_white_powerpoint.png',
	'otp' => 'page_white_powerpoint.png',
	'ott' => 'page_white_powerpoint.png',
	'pages' => 'pages.png',
	'pdf' => 'page_white_acrobat.png',
	'pot' => 'page_white_powerpoint.png',
	'pps' => 'page_white_powerpoint.png',
	'ppsx' => 'page_white_powerpoint.png',
	'ppt' => 'page_white_powerpoint.png',
	'pptx' => 'page_white_powerpoint.png',
	'sig' => 'page_white_text.png',
	'sql' => 'page_white_text.png',
	'txt' => 'page_white_text.png',
	'xl' => 'page_excel.png',
	'xla' => 'page_excel.png',
	'xlb' => 'page_excel.png',
	'xlc' => 'page_excel.png',
	'xld' => 'page_excel.png',
	'xlk' => 'page_excel.png',
	'xll' => 'page_excel.png',
	'xlm' => 'page_excel.png',
	'xls' => 'page_excel.png',
	'xlsx' => 'page_excel.png',
	'xlt' => 'page_excel.png',
	'xlv' => 'page_excel.png',
	'xlw' => 'page_excel.png',
	'zip' => 'page_white_compressed.png',
);


$finfo = new finfo(FILEINFO_MIME_TYPE);

$up = upload($allowuploads,$finfo,$supportedextentions,MS5E_FILES_PATH);
$msg = $up['msg'];
$leadon = $up['leadon'];

$racine = getUrl('js/openwysiwyg_v1.4.7/addons/documentlibrary/');

?>