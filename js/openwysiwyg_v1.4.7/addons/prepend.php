<?php

require_once '../../../../prepend.php';

if(!defined('MS5E_ROOT')) exit;

function allIps() {
	$ip = $_SERVER['REMOTE_ADDR'];
	# On utilise en plus les entêtes HTTP pour empêcher le vol de session par les utilisateurs derrière le même proxy
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip .= '_'.$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	if (isset($_SERVER['HTTP_CLIENT_IP'])) {
		$ip .= '_'.$_SERVER['HTTP_CLIENT_IP'];
	}
	return $ip;
}
function upload($allowuploads,$finfo,$supportedextentions,$MAIN_DIR,$overwrite=false) {
	$msg = '';
	$lead = '';
	if($allowuploads) {
		$phpallowuploads = (bool) ini_get('file_uploads');		
		$phpmaxsize = trim(ini_get('upload_max_filesize'));
		$last = strtolower($phpmaxsize{strlen($phpmaxsize)-1});
		switch($last) {
			case 'g':
				$phpmaxsize *= (1024*1024*1024);
			case 'm':
				$phpmaxsize *= (1024*1024);
		}
	}

	// upload file
	if($allowuploads && isset($_FILES['file'])) {

		$upload = true;
		$lead = trim(nullbyteRemove($_POST['lead']));
		$root = substr($lead, 0, strpos($lead, '/'));
		$ssdir = substr($lead, strpos($lead, '/') + 1);

		try {
		    
		    // Undefined | Multiple Files | $_FILES Corruption Attack
		    // If this request falls under any of them, treat it invalid.
		    if ( !isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
		    	$upload = false;
		        throw new RuntimeException('Invalid parameters.');
		    }

		    // Check $_FILES['file']['error'] value.
		    switch ($_FILES['file']['error']) {
		        case UPLOAD_ERR_OK:
		            break;
		        case UPLOAD_ERR_NO_FILE:
		            throw new RuntimeException('No file sent.');
		        case UPLOAD_ERR_INI_SIZE:
		        case UPLOAD_ERR_FORM_SIZE:
		            throw new RuntimeException(L_FILE.' '.L_TOO_BIG.' (max : '.ini_get('upload_max_filesize').')');
		        default:
		            throw new RuntimeException('Unknown errors.');
		    }

		    // You should also check filesize here. 
		    if ($_FILES['file']['size'] > $phpmaxsize) {
		        $upload = false;
		        throw new RuntimeException(L_FILE.' '.L_TOO_BIG.' (max : '.ini_get('upload_max_filesize').')');
		    }

		    // DO NOT TRUST $_FILES['file']['mime'] VALUE !!
		    // Check MIME Type by yourself.
		    if (false === $ext = array_search($finfo->file($_FILES['file']['tmp_name']),$supportedextentions,true)) {
		        $upload = false;
		        throw new RuntimeException(L_INVALID_TYPE_FILE);
		    }

		    if ($root != null) {
		    	if ($handle = opendir($MAIN_DIR.$root)) {
		    		while (false !== ($file = readdir($handle))) { 
		    			//first see if this file is required in the listing
		    			if ($file == "." || $file == "..")  continue;
		    			if (@filetype($MAIN_DIR.$root.DS.$file) == "dir") {
		    				if(!$browsedirs) continue;
		    				$key++;
		    				$exploreDir[$key] = trim($file) . "/";
		    			}
		    		}
		    		closedir($handle); 
		    	}
		    	if($ssdir != null && $exploreDir != null && !in_array($ssdir, $exploreDir)) {
		    		$upload = false;
		    		throw new RuntimeException('Unknown errors.');
		    	}
		    }

		    if (is_dir($MAIN_DIR.$lead)) {
		    	if($overwrite === false) {
		    		if(file_exists($MAIN_DIR.$lead.$_FILES['file']['name'])) {
		    			$upload = false;
		    			throw new RuntimeException('<span class="error">'.L_FILE.' '.L_ALREADY_EXISTS.'</span>');
		    		}
		    	}
		    	if($upload) {
				    // You should name it uniquely.
				    // DO NOT USE $_FILES['file']['name'] WITHOUT ANY VALIDATION !!
				    // On this example, obtain safe unique name from its binary data.
				    if (!move_uploaded_file($_FILES['file']['tmp_name'], 
				    		sprintf($MAIN_DIR.$lead.'%s.%s',
					            title2filename(str_replace('.'.$ext,'',$_FILES['file']['name'])).'-MS5E-'.sha1_file($_FILES['file']['tmp_name']),
					            $ext
					        ))
				    ) {
				        throw new RuntimeException('Failed to move uploaded file.');
				    }
				}
		    } else {
		    	$lead = '';
		    }

		} catch (RuntimeException $e) { 
			$msg = '<span class="error">'.$e->getMessage().'</span>';
		}
	}
	return array(
		'msg' => $msg,
		'leadon' => $lead,
		'phpallowuploads' => $phpallowuploads
	);
}
?>